﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using BenchmarkDotNet.Attributes;
using Stringularity;
using Stringularity.Stringies.Concats;

namespace Lib.PERFS {
	public class Program {
		public static void Main(string[] args) {
			// BenchmarkDotNet.Running.BenchmarkRunner.Run<Benchmarks_2>();
			RunGross<Benchmarks_2>();
		}

		private static void RunGross<TContainer>() where TContainer : class, new() {
			RunGross<TContainer>(() => new TContainer());
		}
		private static void RunGross<TContainer>(Func<TContainer> containerFactory) {
			var allMethods = typeof(TContainer).GetMethods(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);

			#region Setuping
			var setupMethods = (from m in allMethods
								where m.GetCustomAttribute<GlobalSetupAttribute>() is object
								where m.ReturnType.FullName == "System.Void"
								where m.GetParameters().Length < 1
								select m).ToArray();
			#endregion Setuping

			#region Benchmarking
			var benchmarkMethods = (from m in allMethods
									where m.GetCustomAttributes(false).OfType<BenchmarkAttribute>().Any()
									where m.ReturnType.FullName == "System.Void"
									where m.GetParameters().Length < 1
									select m).ToArray();
			var benchmarkContainers = benchmarkMethods.Select(x => containerFactory()).ToArray();
			var benchmarkActions = benchmarkMethods.Select((x, n) => (Action)Delegate.CreateDelegate(typeof(Action), benchmarkContainers[n], x.Name)).ToArray();
			#endregion Benchmarking

			void PrepareGC() {
				GC.Collect();
				GC.WaitForPendingFinalizers();
				GC.Collect();
				GC.WaitForPendingFinalizers();
				GC.Collect();
				GC.WaitForPendingFinalizers();
			}

			void SetupContainer(TContainer container) {
				foreach (var method in setupMethods) {
					method.Invoke(container, null);
				}
			}

			var results = new Dictionary<MethodInfo, object>();
			for (var i = 0; i < benchmarkActions.Length; i++) {
				var action = benchmarkActions[i];
				var container = benchmarkContainers[i];

				// prepare
				PrepareGC();
				SetupContainer(container);
				PrepareGC();

				// run
				var swatch = new Stopwatch();
				int counterIterations = 0;
				try {
					while (true) {
						counterIterations++;

						swatch.Start();
						action();
						swatch.Stop();

						if (swatch.ElapsedMilliseconds < 10) {
							continue;
						}

						break;
					}
				}
				catch (Exception exc) {
					results[action.Method] = exc;
					continue;
				}
				results[action.Method] = (swatch.Elapsed, counterIterations);
			}

			var len0 = results.Select(x => x.Key.Name.Length).Max();
			Console.Write("Benchmark".PadRight(len0));
			Console.Write(" | ");
			Console.Write("Results");
			Console.WriteLine();

			foreach (var kp in results) {
				Console.Write(kp.Key.Name.PadRight(len0));
				Console.Write(" | ");

				var val = kp.Value;
				if (val is Exception exc) {
					Console.Write("ERR: " + exc.Message);
				}
				else if (val is TimeSpan ts) {
					Console.Write($"{ts.TotalMilliseconds} ms");
				}
				else if (val is ValueTuple<TimeSpan, int> tup) {
					var oneRun = (((decimal)tup.Item1.TotalMilliseconds) / tup.Item2) * 1000;

					Console.Write($"{oneRun} us * {tup.Item2} = {tup.Item1.TotalMilliseconds} ms");
				}
				else {
					Console.Write($"??? {val}");
				}
				Console.WriteLine();
			}
			return;
		}
	}

	[ShortRunJob, CoreJob, MemoryDiagnoser]
	public class Benchmarks_1 {
		private readonly string Hello_String = "Hello";
		private readonly IStringie Hello_Stringie = Stringie.Create("Hello");
		private readonly string World_String = "World";
		private readonly IStringie World_Stringie = Stringie.Create("World");

		[Benchmark]
		public void ConcatedTwoStrings_ByConcat() {
			var str = string.Concat(Hello_String, World_String);
		}

		[Benchmark]
		public void ConcatedTwoStrings_ByPlus() {
			var str = Hello_String + World_String;
		}

		[Benchmark]
		public void ConcatedTwoStringies_FromStrings() {
			var subject = new ConcatedTwoStringies(Hello_String, World_String);
		}

		[Benchmark]
		public void ConcatedTwoStringies_FromStrings_ToString() {
			var subject = new ConcatedTwoStringies(Hello_String, World_String);
			var str = subject.ToString();
		}

		[Benchmark]
		public void ConcatedTwoStringies_FromStringies() {
			var subject = new ConcatedTwoStringies(Hello_Stringie, World_Stringie);
		}

		[Benchmark]
		public void ConcatedTwoStringies_FromStringies_ToString() {
			var subject = new ConcatedTwoStringies(Hello_Stringie, World_Stringie);
			var str = subject.ToString();
		}

		[Benchmark]
		public void ConcatedStringieArray_FromStrings() {
			var arr = new IStringie[] { Stringie.Create(Hello_String), Stringie.Create(World_String) };
			var subject = new ConcatedStringieArray(arr);
		}

		[Benchmark]
		public void ConcatedStringieArray_FromStrings_ToString() {
			var arr = new IStringie[] { Stringie.Create(Hello_String), Stringie.Create(World_String) };
			var subject = new ConcatedStringieArray(arr);
			var str = subject.ToString();
		}
	}

	[ShortRunJob, MemoryDiagnoser]
	public class Benchmarks_2 {
		//[Params(2, 3, 4, 5, 10, 100)]
		[Params(100)]
		public int ConcatedStringsCount = 2;

		private string[] Strings;
		private Stringie[] Stringies;

		[GlobalSetup]
		public void Setup() {
			Strings = Enumerable.Range(0, ConcatedStringsCount).Select(x => "Hello").ToArray();
			Stringies = Strings.Select(Stringie.Create).ToArray();
		}

		[Benchmark]
		public void ConcatedStrings_ByConcat() {
			var str = string.Concat(Strings);
		}

		[Benchmark]
		public void ConcatedStrings_ByPlus() {
			var str = Strings[0];
			for (var i = 1; i < ConcatedStringsCount; i++) {
				str += Strings[i];
			}
		}

		[Benchmark]
		public void ConcatedStringies_FromString_ByPlus() {
			var subject = Stringie.Create(Strings[0]);
			for (var i = 1; i < ConcatedStringsCount; i++) {
				subject += Strings[i];
			}
		}

		[Benchmark]
		public void ConcatedStringies_FromString_ByPlus_ToString() {
			var subject = Stringie.Create(Strings[0]);
			for (var i = 1; i < ConcatedStringsCount; i++) {
				subject += Strings[i];
			}

			var str = subject.ToString();
		}

		[Benchmark]
		public void ConcatedStringies_ByPlus() {
			var subject = Stringies[0];
			for (var i = 1; i < ConcatedStringsCount; i++) {
				subject += Stringies[i];
			}
		}

		[Benchmark]
		public void ConcatedStringies_ByPlus_ToString() {
			var subject = Stringies[0];
			for (var i = 1; i < ConcatedStringsCount; i++) {
				subject += Stringies[i];
			}

			var str = subject.ToString();
		}

		[Benchmark]
		public void ConcatedStringieArray_FromStrings() {
			var arr = new IStringie[Strings.Length];
			for (var i = 0; i < arr.Length; i++) {
				arr[i] = Stringie.Create(Strings[i]);
			}
			var subject = new ConcatedStringieArray(arr);
		}

		[Benchmark]
		public void ConcatedStringieArray_FromStrings_ToString() {
			var arr = new IStringie[Strings.Length];
			for (var i = 0; i < arr.Length; i++) {
				arr[i] = Stringie.Create(Strings[i]);
			}
			var subject = new ConcatedStringieArray(arr);
			var str = subject.ToString();
		}

		[Benchmark]
		public void ConcatedStringieArray() {
			var subject = new ConcatedStringieArray(Stringies);
		}

		[Benchmark]
		public void ConcatedStringieArray_ToString() {
			var subject = new ConcatedStringieArray(Stringies);
			var str = subject.ToString();
		}
	}
}
