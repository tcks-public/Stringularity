﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Stringularity.Checks;

namespace Stringularity {
	public static class TestHelper {
		public sealed class CommonVariants {
			public readonly Variants Null = Vary(null);
			public readonly Variants Empty = Vary(string.Empty);
			public readonly Variants Alice = Vary("Alice");
			public readonly Variants White = Vary("\r\n\t ");
			public readonly Variants LongWhite = Vary("\r\n\t \r\n\t \r\n\t \r\n\t \r\n\t \r\n\t \r\n\t \r\n\t \r\n\t \r\n\t \r\n\t \r\n\t \r\n\t \r\n\t ");

			public IEnumerable<Variants> Enum() {
				yield return Null;
				yield return Empty;
				yield return Alice;
				yield return White;
				yield return LongWhite;
			}
		}

		public sealed class Variants {
			public readonly string String;
			public readonly Stringie Stringie;
			public readonly char[] CharArray;
			public readonly IReadOnlyList<char> CharList;

			public Variants(string value) {
				this.String = value;
				this.Stringie = value;
				this.CharArray = value?.ToCharArray();
				this.CharList = value?.ToCharArray()?.ToList();
			}
		}

		public static Variants Vary(string str) => new Variants(str);

		public static IEnumerable<StringComparison> GetStringComparisons() => Enum.GetValues(typeof(StringComparison)).Cast<StringComparison>();
		public static void ForEachStringComparision(Action<StringComparison> check) {
			if (check is null) { throw new ArgumentNullException(nameof(check)); }

			foreach (var comp in GetStringComparisons()) {
				check(comp);
			}
		}

		public static void RunWithSharedTests(IStringie subject, IChecks checks) {
			if (subject is null) { throw new ArgumentNullException(nameof(subject)); }
			if (checks is null) { throw new ArgumentNullException(nameof(checks)); }

			RunSharedTests(subject);

			checks.CheckAll(subject);
		}

		public static void RunWithSharedTests(IEnumerable<IStringie> subjects, IChecks checks) {
			if (subjects is null) { throw new ArgumentNullException(nameof(subjects)); }
			if (checks is null) { throw new ArgumentNullException(nameof(checks)); }

			int counter = 0;
			foreach (var subject in subjects) {
				if (subject is null) { continue; }

				counter++;
				RunWithSharedTests(subject, checks);
			}

			if (counter < 1) {
				throw new ArgumentException("Non empty subject are required.");
			}
		}

		public static void RunSharedTests(IEnumerable<IStringie> subjects) {
			MultiChecks.CreateDefault().CheckAll(subjects);
		}
		public static void RunSharedTests(IStringie subject) {
			MultiChecks.CreateDefault().CheckAll(subject);
		}

		public static void AssertEquality(IStringie expected, IStringie actual) {
			Assert.AreEqual(expected, actual);

			Assert.AreEqual(expected.IsEmpty, actual.IsEmpty);
			Assert.AreEqual(expected.IsWhitespace, actual.IsWhitespace);
			Assert.AreEqual(expected.Length, actual.Length);
			Assert.AreEqual(expected.ToString(), actual.ToString());
			Assert.AreEqual(expected.GetHashCode(), actual.GetHashCode());

			Assert.IsTrue(expected.Equals(actual));
			Assert.IsTrue(actual.Equals(expected));

			Assert.AreEqual(0, expected.CompareTo(actual));
			Assert.AreEqual(0, actual.CompareTo(expected));

			Assert.IsTrue(expected.Contains(actual));
			Assert.IsTrue(actual.Contains(expected));

			Assert.IsTrue(expected.EndsWith(actual));
			Assert.IsTrue(actual.EndsWith(expected));

			Assert.AreEqual(0, expected.IndexOf(actual));
			Assert.AreEqual(0, actual.IndexOf(expected));

			Assert.IsTrue(expected.StartsWith(actual));
			Assert.IsTrue(actual.StartsWith(expected));
		}

		public static IEnumerable<string> GenerateShortersOrNull(string original) {
			if (string.IsNullOrEmpty(original)) { return null; }

			return GenerateShortersFromEnd(original);
		}

		public static IEnumerable<string> GenerateShortersFromEnd(string original) {
			if (string.IsNullOrEmpty(original)) { yield break; }

			for (var i = 0; i < original.Length - 1; i++) {
				var shorter = original.Substring(0, original.Length - (i + 1));
				yield return shorter;
			}
		}

		public static IEnumerable<string> GenerateShortersFromStart(string original) {
			if (string.IsNullOrEmpty(original)) { yield break; }

			for (var i = 0; i < original.Length - 1; i++) {
				var shorter = original.Substring(i + 1);
				yield return shorter;
			}
		}

		public static IEnumerable<string> GenerateShortersFromBothEnds(string original) {
			if (string.IsNullOrEmpty(original)) { yield break; }

			var str = original;
			while (str.Length > 2) {
				str = str.Substring(1);
				str = str.Substring(0, str.Length - 1);
				yield return str;
			}
		}

		public static IEnumerable<string> GenerateAppends(string original, int count) {
			var str = original ?? string.Empty;
			for (var i = 0; i < count; i++) {
				yield return str += (char)i;
			}
		}

		public static IEnumerable<string> GenerateOneCharHigher(string original) {
			if (string.IsNullOrEmpty(original)) { yield break; }

			var arrOrig = original.ToCharArray();
			for (var i = 0; i < arrOrig.Length; i++) {
				var arrNext = (char[])arrOrig;
				arrNext[i] = (char)(arrOrig[i] + 1);

				yield return new string(arrNext);
			}
		}

		public static IEnumerable<string> GenerateOneCharLower(string original) {
			if (string.IsNullOrEmpty(original)) { yield break; }

			var arrOrig = original.ToCharArray();
			for (var i = 0; i < arrOrig.Length; i++) {
				var arrNext = (char[])arrOrig;
				arrNext[i] = (char)(arrOrig[i] - 1);

				yield return new string(arrNext);
			}
		}
	}
}
