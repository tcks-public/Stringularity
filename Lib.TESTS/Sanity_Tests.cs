﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity {
	using static TestHelper;

	[TestClass]
	public class Sanity_Tests {
		[TestMethod]
		public void GenerateAppendsWorks() {
			{
				var arrZero = GenerateAppends("asdf", 0).ToArray();
				Assert.AreEqual(0, arrZero.Length);
			}

			{
				var arrOne = GenerateAppends("asdf", 1).ToArray();
				Assert.AreEqual(1, arrOne.Length);
				Assert.AreEqual(("asdf" + (char)0), arrOne[0]);
			}

			{
				var arrTen = GenerateAppends("asdf", 10).ToArray();
				Assert.AreEqual(10, arrTen.Length);

				var str = "asdf";
				Assert.AreEqual((str += (char)0), arrTen[0]);
				Assert.AreEqual((str += (char)1), arrTen[1]);
				Assert.AreEqual((str += (char)2), arrTen[2]);
				Assert.AreEqual((str += (char)3), arrTen[3]);
				Assert.AreEqual((str += (char)4), arrTen[4]);
				Assert.AreEqual((str += (char)5), arrTen[5]);
				Assert.AreEqual((str += (char)6), arrTen[6]);
				Assert.AreEqual((str += (char)7), arrTen[7]);
				Assert.AreEqual((str += (char)8), arrTen[8]);
				Assert.AreEqual((str += (char)9), arrTen[9]);
			}
		}

		[TestMethod]
		public void GenerateShortersFromEndWorks() {
			{
				var arrZero = GenerateShortersFromEnd(null).ToArray();
				Assert.AreEqual(0, arrZero.Length);
			}

			{
				var arrZero = GenerateShortersFromEnd(string.Empty).ToArray();
				Assert.AreEqual(0, arrZero.Length);
			}

			{
				var arrOne = GenerateShortersFromEnd("X").ToArray();
				Assert.AreEqual(0, arrOne.Length);
			}

			{
				var arrTwo = GenerateShortersFromEnd("XY").ToArray();
				Assert.AreEqual(1, arrTwo.Length);
				Assert.AreEqual("X", arrTwo[0]);
			}

			{
				var arrTen = GenerateShortersFromEnd("1234567890").ToArray();
				Assert.AreEqual(9, arrTen.Length);
				Assert.AreEqual("123456789", arrTen[0]);
				Assert.AreEqual("12345678", arrTen[1]);
				Assert.AreEqual("1234567", arrTen[2]);
				Assert.AreEqual("123456", arrTen[3]);
				Assert.AreEqual("12345", arrTen[4]);
				Assert.AreEqual("1234", arrTen[5]);
				Assert.AreEqual("123", arrTen[6]);
				Assert.AreEqual("12", arrTen[7]);
				Assert.AreEqual("1", arrTen[8]);
			}
		}

		[TestMethod]
		public void GenerateShortersFromStartWorks() {
			{
				var arrZero = GenerateShortersFromStart(null).ToArray();
				Assert.AreEqual(0, arrZero.Length);
			}

			{
				var arrZero = GenerateShortersFromStart(string.Empty).ToArray();
				Assert.AreEqual(0, arrZero.Length);
			}

			{
				var arrOne = GenerateShortersFromStart("X").ToArray();
				Assert.AreEqual(0, arrOne.Length);
			}

			{
				var arrTwo = GenerateShortersFromStart("XY").ToArray();
				Assert.AreEqual(1, arrTwo.Length);
				Assert.AreEqual("Y", arrTwo[0]);
			}

			{
				var arrTen = GenerateShortersFromStart("1234567890").ToArray();
				Assert.AreEqual(9, arrTen.Length);
				Assert.AreEqual("234567890", arrTen[0]);
				Assert.AreEqual("34567890", arrTen[1]);
				Assert.AreEqual("4567890", arrTen[2]);
				Assert.AreEqual("567890", arrTen[3]);
				Assert.AreEqual("67890", arrTen[4]);
				Assert.AreEqual("7890", arrTen[5]);
				Assert.AreEqual("890", arrTen[6]);
				Assert.AreEqual("90", arrTen[7]);
				Assert.AreEqual("0", arrTen[8]);
			}
		}

		[TestMethod]
		public void GenerateShortersFromBothEndsWorks() {
			{
				var arrZero = GenerateShortersFromBothEnds(null).ToArray();
				Assert.AreEqual(0, arrZero.Length);
			}

			{
				var arrZero = GenerateShortersFromBothEnds(string.Empty).ToArray();
				Assert.AreEqual(0, arrZero.Length);
			}

			{
				var arrOne = GenerateShortersFromBothEnds("A").ToArray();
				Assert.AreEqual(0, arrOne.Length);
			}

			{
				var arrTwo = GenerateShortersFromBothEnds("AB").ToArray();
				Assert.AreEqual(0, arrTwo.Length);
			}

			{
				var arrThree = GenerateShortersFromBothEnds("ABC").ToArray();
				Assert.AreEqual(1, arrThree.Length);
				Assert.AreEqual("B", arrThree[0]);
			}

			{
				var arrFour = GenerateShortersFromBothEnds("ABCD").ToArray();
				Assert.AreEqual(1, arrFour.Length);
				Assert.AreEqual("BC", arrFour[0]);
			}

			{
				var arrFive = GenerateShortersFromBothEnds("ABCDE").ToArray();
				Assert.AreEqual(2, arrFive.Length);
				Assert.AreEqual("BCD", arrFive[0]);
				Assert.AreEqual("C", arrFive[1]);
			}

			{
				var arrTen = GenerateShortersFromBothEnds("ABCDEFGHJK").ToArray();
				Assert.AreEqual(4, arrTen.Length);
				Assert.AreEqual("BCDEFGHJ", arrTen[0]);
				Assert.AreEqual("CDEFGH", arrTen[1]);
				Assert.AreEqual("DEFG", arrTen[2]);
				Assert.AreEqual("EF", arrTen[3]);
			}

			{
				var arrEleven = GenerateShortersFromBothEnds("ABCDEFGHJKL").ToArray();
				Assert.AreEqual(5, arrEleven.Length);
				Assert.AreEqual("BCDEFGHJK", arrEleven[0]);
				Assert.AreEqual("CDEFGHJ", arrEleven[1]);
				Assert.AreEqual("DEFGH", arrEleven[2]);
				Assert.AreEqual("EFG", arrEleven[3]);
				Assert.AreEqual("F", arrEleven[4]);
			}
		}

		[TestMethod]
		public void ForEachComparisionWorks() {
			var allComparisions = GetStringComparisons().ToArray();
			Assert.IsTrue(allComparisions.Length > 0);

			var counters = allComparisions.ToDictionary(x => x, x => 0);
			ForEachStringComparision(comp => {
				counters[comp]++;
			});

			Assert.AreEqual(allComparisions.Length, counters.Count);
			foreach (var comp in allComparisions) {
				Assert.AreEqual(1, counters[comp]);
			}
		}
	}
}
