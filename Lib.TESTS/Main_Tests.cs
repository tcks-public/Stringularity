﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Stringularity.Stringies;

namespace Stringularity {
    using Stringularity.Checks.Specialized;
    using Stringularity.Checks.Specialized.Multis;
    using Stringularity.Stringies.Multis;
    using static Stringularity.TestHelper;

	[TestClass]
	public class Main_Tests {
		[TestMethod]
		public void EmptyWorks() {
			var subject = new Empty();

			RunWithSharedTests(subject, EmptyChecks.Default);
		}

		[TestMethod]
		public void OneCharWorks() {
			var subjects = "Hello world!".Select(x => (Stringie)x).ToArray();

			RunWithSharedTests(subjects, OneCharChecks.Default);
		}

		[TestMethod]
		public void CharArrayWorks() {
			var subject = new CharArray("Hello world!".ToCharArray());

			RunWithSharedTests(subject, CharArrayChecks.Default);
		}

		[TestMethod]
		public void OneStringWorks() {
			var subject = new OneString("Hello world!");

			RunWithSharedTests(subject, OneStringChecks.Default);
		}

		[TestMethod]
		public void MultipliedCharWorks() {
			foreach (var ch in "ABCDEFGH".ToCharArray()) {
				for (var i = 2; i < 50; i++) {
					var subject = new MultipliedChar(ch, i);

					RunWithSharedTests(subject, MultipliedCharChecks.Default);
				}
			}
		}
	}
}
