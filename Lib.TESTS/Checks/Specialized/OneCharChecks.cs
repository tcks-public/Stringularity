﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Specialized {
	public sealed class OneCharChecks : ClassLevelChecks {
		private static OneCharChecks @default;
		public static OneCharChecks Default => @default ?? (@default = new OneCharChecks());

		public override void CheckProperties(IStringie subject) {
			Assert.IsFalse(subject.IsComposed);
			Assert.IsFalse(subject.IsEmpty);
			Assert.AreEqual(1, subject.Length);
		}

		public override void CheckFlatten(IStringie subject) {
			base.CheckFlatten(subject);

			var flat = subject.Flatten();
			Assert.AreSame(subject, flat);
		}
	}
}
