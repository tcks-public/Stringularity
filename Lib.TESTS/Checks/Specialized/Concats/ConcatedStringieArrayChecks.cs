﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity.Checks.Specialized.Concats {
	public sealed class ConcatedStringieArrayChecks : ConcatedClassLevelChecks {
		private static ConcatedStringieArrayChecks @default;
		public static ConcatedStringieArrayChecks Default => @default ?? (@default = new ConcatedStringieArrayChecks());
	}
}
