﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Specialized.Concats {
	public abstract class ConcatedClassLevelChecks : ClassLevelChecks {
		public override void CheckProperties(IStringie subject) {
			base.CheckProperties(subject);

			Assert.IsTrue(subject.IsComposed);
		}

		public override void CheckFlatten(IStringie subject) {
			base.CheckFlatten(subject);

			var flat = subject.Flatten();
			Assert.AreNotSame(subject, flat);
			Assert.IsFalse(flat.IsComposed);
		}
	}
}
