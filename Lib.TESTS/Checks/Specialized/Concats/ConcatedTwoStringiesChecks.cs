﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity.Checks.Specialized.Concats {
	public sealed class ConcatedTwoStringiesChecks : ConcatedClassLevelChecks {
		private static ConcatedTwoStringiesChecks @default;
		public static ConcatedTwoStringiesChecks Default => @default ?? (@default = new ConcatedTwoStringiesChecks());
	}
}
