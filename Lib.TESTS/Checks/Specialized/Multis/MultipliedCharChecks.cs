﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity.Checks.Specialized.Multis {
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using static TestHelper;

	public sealed class MultipliedCharChecks : ClassLevelChecks {
		private static MultipliedCharChecks @default;
		public static MultipliedCharChecks Default => @default ?? (@default = new MultipliedCharChecks());

		public override void CheckAll(IStringie subject) {
			base.CheckAll(subject);

			CheckContent(subject);
		}

		public void CheckContent(IStringie subject) {
			var len = subject.Length;
			if (len < 1) { return; }

			var ch = subject[0];
			for (var i = 1; i < len; i++) {
				Assert.AreEqual(ch, subject[i]);
			}
 		}
	}
}
