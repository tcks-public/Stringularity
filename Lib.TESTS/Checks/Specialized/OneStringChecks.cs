﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Specialized {
	public sealed class OneStringChecks : ClassLevelChecks {
		private static OneStringChecks @default;
		public static OneStringChecks Default => @default ?? (@default = new OneStringChecks());

		public override void CheckProperties(IStringie subject) {
			base.CheckProperties(subject);

			Assert.IsFalse(subject.IsComposed);
		}

		public override void CheckFlatten(IStringie subject) {
			base.CheckFlatten(subject);

			var flat = subject.Flatten();
			Assert.AreSame(subject, flat);
		}
	}
}
