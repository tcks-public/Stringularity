﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Specialized {
	using static TestHelper;

	public sealed class EmptyChecks : ClassLevelChecks {
		private static EmptyChecks @default;
		public static EmptyChecks Default => @default ?? (@default = new EmptyChecks());

		public override void CheckProperties(IStringie subject) {
			base.CheckProperties(subject);

			Assert.IsFalse(subject.IsWhitespace);
			Assert.IsTrue(subject.IsEmpty);
			Assert.AreEqual(0, subject.Length);
			Assert.IsFalse(subject.IsComposed);
			Assert.AreSame(subject, subject.Flatten());
			Assert.AreEqual(0, subject.ToString().Length);
		}

		public override void CheckFlatten(IStringie subject) {
			base.CheckFlatten(subject);

			var flat = subject.Flatten();

			Assert.AreSame(subject, flat);
		}
	}
}
