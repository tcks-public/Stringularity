﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity.Checks {
	public abstract class ClassLevelChecks : ChecksBase {
		public override void CheckAll(IStringie subject) {
			CheckProperties(subject);
			CheckClone(subject);
			CheckCompareTo(subject);
			CheckContains(subject);
			CheckEndsWith(subject);
			CheckEquals(subject);
			CheckFlatten(subject);
			CheckGetHashCode(subject);
			CheckIndexOf(subject);
			CheckStartsWith(subject);
		}

		public virtual void CheckProperties(IStringie subject) {
			// do nothing
		}

		public virtual void CheckClone(IStringie subject) {
			// do nothing
		}
		public virtual void CheckCompareTo(IStringie subject) {
			// do nothing
		}
		public virtual void CheckContains(IStringie subject) {
			// do nothing
		}
		public virtual void CheckEndsWith(IStringie subject) {
			// do nothing
		}
		public virtual void CheckEquals(IStringie subject) {
			// do nothing
		}
		public virtual void CheckFlatten(IStringie subject) {
			// do nothing
		}
		public virtual void CheckGetHashCode(IStringie subject) {
			// do nothing
		}
		public virtual void CheckIndexOf(IStringie subject) {
			// do nothing
		}
		public virtual void CheckStartsWith(IStringie subject) {
			// do nothing
		}
	}
}
