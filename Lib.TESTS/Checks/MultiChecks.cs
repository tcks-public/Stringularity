﻿using System;
using System.Collections.Generic;
using System.Text;

using Stringularity.Checks.Shared;
using Stringularity.Checks.Shared.MemberLevel;

namespace Stringularity.Checks {
	public sealed class MultiChecks : ChecksBase {
		public static MultiChecks CreateDefault() => new MultiChecks {
			Checks = {
				NullConsistencyChecks.Default,
				EmptyConsistencyChecks.Default,
				SelfConsistencyChecks.Default,

				CloneChecks.Default,
				CompareToChecks.Default,
				ContainsChecks.Default,
				EndsWithChecks.Default,
				EqualsChecks.Default,
				FlattenChecks.Default,
				IndexOfChecks.Default,
				StartsWithChecks.Default,

				AppendChecks.Default,
				PrependChecks.Default,
				PadLeftChecks.Default
			}
		};

		public IList<IChecks> Checks { get; } = new List<IChecks>();

		public override void CheckAll(IStringie subject) {
			if (subject is null) { throw new ArgumentNullException(nameof(subject)); }

			int counter = 0;
			foreach (var check in this.Checks) {
				if (check is null) { continue; }

				check.CheckAll(subject);
				counter++;
			}

			if (counter < 1) {
				throw new InvalidOperationException("No checks to run.");
			}
		}
	}
}
