﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity.Checks {
	public abstract class ChecksBase : IChecks {
		public abstract void CheckAll(IStringie subject);
		public virtual void CheckAll(IEnumerable<IStringie> subjects) {
			if (subjects is null) { throw new ArgumentNullException(nameof(subjects)); }

			int counter = 0;
			foreach (var subject in subjects) {
				this.CheckAll(subject);
				counter++;
			}

			if (counter < 1) {
				throw new ArgumentException("Empty subjects.", nameof(subjects));
			}
		}
	}
}
