﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity.Checks {
	public interface IChecks {
		void CheckAll(IStringie subject);
		void CheckAll(IEnumerable<IStringie> subjects);
	}
}
