﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Shared {
	using static TestHelper;

	public sealed class EmptyConsistencyChecks : ChecksBase {
		private static EmptyConsistencyChecks @default;
		public static EmptyConsistencyChecks Default => @default ?? (@default = new EmptyConsistencyChecks());

		public override void CheckAll(IStringie subject) {
			if (subject is null) { throw new ArgumentNullException(nameof(subject)); }

			var vars = Vary(string.Empty);

			#region CompareTo
			Assert.AreEqual(subject.CompareTo(vars.String), subject.CompareTo(vars.String));
			Assert.AreEqual(subject.CompareTo(vars.Stringie), subject.CompareTo(vars.Stringie));
			Assert.AreEqual(subject.CompareTo(vars.CharArray), subject.CompareTo(vars.CharArray));
			Assert.AreEqual(subject.CompareTo(vars.CharList), subject.CompareTo(vars.CharList));
			#endregion CompareTo

			#region Contains
			Assert.IsTrue(subject.Contains(vars.String));
			Assert.IsTrue(subject.Contains(vars.Stringie));
			Assert.IsTrue(subject.Contains(vars.CharArray));
			Assert.IsTrue(subject.Contains(vars.CharList));

			ForEachStringComparision(comp => {
				Assert.IsTrue(subject.Contains(vars.String, comp));
				Assert.IsTrue(subject.Contains(vars.Stringie, comp));
				Assert.IsTrue(subject.Contains(vars.CharArray, comp));
				Assert.IsTrue(subject.Contains(vars.CharList, comp));
			});
			#endregion Contains

			#region EndsWith
			Assert.IsTrue(subject.EndsWith(vars.String));
			Assert.IsTrue(subject.EndsWith(vars.Stringie));
			Assert.IsTrue(subject.EndsWith(vars.CharArray));
			Assert.IsTrue(subject.EndsWith(vars.CharList));

			ForEachStringComparision(comp => {
				Assert.IsTrue(subject.EndsWith(vars.String, comp));
				Assert.IsTrue(subject.EndsWith(vars.Stringie, comp));
				Assert.IsTrue(subject.EndsWith(vars.CharArray, comp));
				Assert.IsTrue(subject.EndsWith(vars.CharList, comp));
			});
			#endregion EndsWith

			#region Equals
			{
				var result = new[]{
					subject.Equals(vars.String),
					subject.Equals((object)vars.String),
					subject.Equals(vars.Stringie),
					subject.Equals((object)vars.Stringie),
					subject.Equals(vars.CharArray),
					subject.Equals((object)vars.CharArray),
					subject.Equals(vars.CharList),
					subject.Equals((object)vars.CharList)
				};

				for (var i = 1; i < result.Length; i++) {
					Assert.AreEqual(result[0], result[i]);
				}
			}
			#endregion Equals

			#region EndsWith
			Assert.IsTrue(subject.EndsWith(vars.String));
			Assert.IsTrue(subject.EndsWith(vars.Stringie));
			Assert.IsTrue(subject.EndsWith(vars.CharArray));
			Assert.IsTrue(subject.EndsWith(vars.CharList));

			ForEachStringComparision(comp => {
				Assert.IsTrue(subject.EndsWith(vars.String, comp));
				Assert.IsTrue(subject.EndsWith(vars.Stringie, comp));
				Assert.IsTrue(subject.EndsWith(vars.CharArray, comp));
				Assert.IsTrue(subject.EndsWith(vars.CharList, comp));
			});
			#endregion EndsWith

			#region IndexOf
			Assert.AreEqual(0, subject.IndexOf(vars.String));
			Assert.AreEqual(0, subject.IndexOf(vars.Stringie));
			Assert.AreEqual(0, subject.IndexOf(vars.CharArray));
			Assert.AreEqual(0, subject.IndexOf(vars.CharList));

			ForEachStringComparision(comp => {
				Assert.AreEqual(0, subject.IndexOf(vars.String, comp));
				Assert.AreEqual(0, subject.IndexOf(vars.Stringie, comp));
				Assert.AreEqual(0, subject.IndexOf(vars.CharArray, comp));
				Assert.AreEqual(0, subject.IndexOf(vars.CharList, comp));
			});
			#endregion IndexOf

			#region StartsWith
			Assert.IsTrue(subject.StartsWith(vars.String));
			Assert.IsTrue(subject.StartsWith(vars.Stringie));
			Assert.IsTrue(subject.StartsWith(vars.CharArray));
			Assert.IsTrue(subject.StartsWith(vars.CharList));

			ForEachStringComparision(comp => {
				Assert.IsTrue(subject.StartsWith(vars.String, comp));
				Assert.IsTrue(subject.StartsWith(vars.Stringie, comp));
				Assert.IsTrue(subject.StartsWith(vars.CharArray, comp));
				Assert.IsTrue(subject.StartsWith(vars.CharList, comp));
			});
			#endregion StartsWith
		}
	}
}
