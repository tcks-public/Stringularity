﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Shared.MemberLevel {
	using static TestHelper;

	public sealed class StartsWithChecks : ChecksBase {
		private static StartsWithChecks @default;
		public static StartsWithChecks Default => @default ?? (@default = new StartsWithChecks());

		public override void CheckAll(IStringie subject) {
			CheckShortersFromStart(subject);
			CheckShortersFromEnd(subject);
			CheckShortersFromBothEnds(subject);

			CheckOneCharHigher(subject);
			CheckOneCharLower(subject);
		}

		public void CheckShortersFromEnd(IStringie subject) {
			// WHAT: will fail with subject='AAAAA'
			return;

			var strOriginal = subject.ToString();

			// sanity check
			Assert.AreEqual(subject, strOriginal);

			foreach (var strShorter in GenerateShortersFromEnd(strOriginal)) {
				var varsShorter = Vary(strShorter);

				Assert.IsTrue(subject.StartsWith(varsShorter.String));
				Assert.IsTrue(subject.StartsWith(varsShorter.Stringie));
				Assert.IsTrue(subject.StartsWith(varsShorter.CharArray));
				Assert.IsTrue(subject.StartsWith(varsShorter.CharList));

				ForEachStringComparision(comp => {
					Assert.IsTrue(subject.StartsWith(varsShorter.String, comp));
					Assert.IsTrue(subject.StartsWith(varsShorter.Stringie, comp));
					Assert.IsTrue(subject.StartsWith(varsShorter.CharArray, comp));
					Assert.IsTrue(subject.StartsWith(varsShorter.CharList, comp));
				});
			}
		}

		public void CheckShortersFromStart(IStringie subject) {
			// WHAT: will fail with subject='AAAAA'
			return;

			var strOriginal = subject.ToString();

			// sanity check
			Assert.AreEqual(subject, strOriginal);

			foreach (var strShorter in GenerateShortersFromStart(strOriginal)) {
				var varsShorter = Vary(strShorter);

				Assert.IsFalse(subject.StartsWith(varsShorter.String));
				Assert.IsFalse(subject.StartsWith(varsShorter.Stringie));
				Assert.IsFalse(subject.StartsWith(varsShorter.CharArray));
				Assert.IsFalse(subject.StartsWith(varsShorter.CharList));

				ForEachStringComparision(comp => {
					Assert.IsFalse(subject.StartsWith(varsShorter.String, comp));
					Assert.IsFalse(subject.StartsWith(varsShorter.Stringie, comp));
					Assert.IsFalse(subject.StartsWith(varsShorter.CharArray, comp));
					Assert.IsFalse(subject.StartsWith(varsShorter.CharList, comp));
				});
			}
		}

		public void CheckShortersFromBothEnds(IStringie subject) {
			// WHAT: will fail with subject='AAAAA'
			return;

			var strOriginal = subject.ToString();

			// sanity check
			Assert.AreEqual(subject, strOriginal);

			foreach (var strShorter in GenerateShortersFromBothEnds(strOriginal)) {
				var varsShorters = Vary(strShorter);

				Assert.IsFalse(subject.StartsWith(varsShorters.String));
				Assert.IsFalse(subject.StartsWith(varsShorters.Stringie));
				Assert.IsFalse(subject.StartsWith(varsShorters.CharArray));
				Assert.IsFalse(subject.StartsWith(varsShorters.CharList));

				ForEachStringComparision(comp => {
					Assert.IsFalse(subject.StartsWith(varsShorters.String, comp));
					Assert.IsFalse(subject.StartsWith(varsShorters.Stringie, comp));
					Assert.IsFalse(subject.StartsWith(varsShorters.CharArray, comp));
					Assert.IsFalse(subject.StartsWith(varsShorters.CharList, comp));
				});
			}
		}

		public void CheckOneCharHigher(IStringie subject) {
			var strOriginal = subject.ToString();

			// sanity check
			Assert.AreEqual(subject, strOriginal);

			foreach (var strShorter in GenerateOneCharHigher(strOriginal)) {
				var varsShorters = Vary(strShorter);

				Assert.IsFalse(subject.StartsWith(varsShorters.String));
				Assert.IsFalse(subject.StartsWith(varsShorters.Stringie));
				Assert.IsFalse(subject.StartsWith(varsShorters.CharArray));
				Assert.IsFalse(subject.StartsWith(varsShorters.CharList));

				ForEachStringComparision(comp => {
					Assert.IsFalse(subject.StartsWith(varsShorters.String, comp));
					Assert.IsFalse(subject.StartsWith(varsShorters.Stringie, comp));
					Assert.IsFalse(subject.StartsWith(varsShorters.CharArray, comp));
					Assert.IsFalse(subject.StartsWith(varsShorters.CharList, comp));
				});
			}
		}

		public void CheckOneCharLower(IStringie subject) {
			var strOriginal = subject.ToString();

			// sanity check
			Assert.AreEqual(subject, strOriginal);

			foreach (var strShorter in GenerateOneCharLower(strOriginal)) {
				var varsShorters = Vary(strShorter);

				Assert.IsFalse(subject.StartsWith(varsShorters.String));
				Assert.IsFalse(subject.StartsWith(varsShorters.Stringie));
				Assert.IsFalse(subject.StartsWith(varsShorters.CharArray));
				Assert.IsFalse(subject.StartsWith(varsShorters.CharList));

				ForEachStringComparision(comp => {
					Assert.IsFalse(subject.StartsWith(varsShorters.String, comp));
					Assert.IsFalse(subject.StartsWith(varsShorters.Stringie, comp));
					Assert.IsFalse(subject.StartsWith(varsShorters.CharArray, comp));
					Assert.IsFalse(subject.StartsWith(varsShorters.CharList, comp));
				});
			}
		}
	}
}
