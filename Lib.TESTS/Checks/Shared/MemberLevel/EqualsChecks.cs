﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Shared.MemberLevel {
	using static TestHelper;

	public sealed class EqualsChecks : ChecksBase {
		private static EqualsChecks @default;
		public static EqualsChecks Default => @default ?? (@default = new EqualsChecks());

		public override void CheckAll(IStringie subject) {
			CheckEqual(subject);
			CheckShortersFromStart(subject);
			CheckShortersFromEnd(subject);
			CheckShortersFromBothEnds(subject);
		}

		public void CheckEqual(IStringie subject) {
			var strOrig = subject.ToString();

			// sanity check
			Assert.AreEqual(strOrig, subject.ToString());

			var strCheck = new string(strOrig.Reverse().ToArray().Reverse().ToArray());
			var varsCheck = Vary(strCheck);

			Assert.IsTrue(subject.Equals(varsCheck.String));
			Assert.IsTrue(subject.Equals((object)varsCheck.String));
			Assert.IsTrue(subject.Equals(varsCheck.Stringie));
			Assert.IsTrue(subject.Equals((object)varsCheck.Stringie));
			Assert.IsTrue(subject.Equals(varsCheck.CharArray));
			Assert.IsTrue(subject.Equals((object)varsCheck.CharArray));
			Assert.IsTrue(subject.Equals(varsCheck.CharList));
			Assert.IsTrue(subject.Equals((object)varsCheck.CharList));
		}

		public void CheckShortersFromStart(IStringie subject) {
			var strOrig = subject.ToString();

			// sanity check
			Assert.AreEqual(strOrig, subject.ToString());

			foreach (var strShorter in GenerateShortersFromStart(strOrig)) {
				var varsShorter = Vary(strShorter);

				Assert.IsFalse(subject.Equals(varsShorter.String));
				Assert.IsFalse(subject.Equals((object)varsShorter.String));
				Assert.IsFalse(subject.Equals(varsShorter.Stringie));
				Assert.IsFalse(subject.Equals((object)varsShorter.Stringie));
				Assert.IsFalse(subject.Equals(varsShorter.CharArray));
				Assert.IsFalse(subject.Equals((object)varsShorter.CharArray));
				Assert.IsFalse(subject.Equals(varsShorter.CharList));
				Assert.IsFalse(subject.Equals((object)varsShorter.CharList));
			}
		}

		public void CheckShortersFromEnd(IStringie subject) {
			var strOrig = subject.ToString();

			// sanity check
			Assert.AreEqual(strOrig, subject.ToString());

			foreach (var strShorter in GenerateShortersFromEnd(strOrig)) {
				var varsShorter = Vary(strShorter);

				Assert.IsFalse(subject.Equals(varsShorter.String));
				Assert.IsFalse(subject.Equals((object)varsShorter.String));
				Assert.IsFalse(subject.Equals(varsShorter.Stringie));
				Assert.IsFalse(subject.Equals((object)varsShorter.Stringie));
				Assert.IsFalse(subject.Equals(varsShorter.CharArray));
				Assert.IsFalse(subject.Equals((object)varsShorter.CharArray));
				Assert.IsFalse(subject.Equals(varsShorter.CharList));
				Assert.IsFalse(subject.Equals((object)varsShorter.CharList));
			}
		}

		public void CheckShortersFromBothEnds(IStringie subject) {
			var strOrig = subject.ToString();

			// sanity check
			Assert.AreEqual(strOrig, subject.ToString());

			foreach (var strShorter in GenerateShortersFromBothEnds(strOrig)) {
				var varsShorter = Vary(strShorter);

				Assert.IsFalse(subject.Equals(varsShorter.String));
				Assert.IsFalse(subject.Equals((object)varsShorter.String));
				Assert.IsFalse(subject.Equals(varsShorter.Stringie));
				Assert.IsFalse(subject.Equals((object)varsShorter.Stringie));
				Assert.IsFalse(subject.Equals(varsShorter.CharArray));
				Assert.IsFalse(subject.Equals((object)varsShorter.CharArray));
				Assert.IsFalse(subject.Equals(varsShorter.CharList));
				Assert.IsFalse(subject.Equals((object)varsShorter.CharList));
			}
		}
	}
}
