﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Shared.MemberLevel {
	using static TestHelper;

	public sealed class FlattenChecks : ChecksBase {
		private static FlattenChecks @default;
		public static FlattenChecks Default => @default ?? (@default = new FlattenChecks());

		public override void CheckAll(IStringie subject) {
			CheckEquality(subject);
		}

		public void CheckEquality(IStringie subject) {
			// note - flatten object can be same instance as original object
			var flat = subject.Flatten();

			AssertEquality(subject, flat);
		}
	}
}
