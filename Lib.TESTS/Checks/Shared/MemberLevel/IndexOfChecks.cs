﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity.Checks.Shared.MemberLevel {
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using static TestHelper;

	public sealed class IndexOfChecks : ChecksBase {
		private static IndexOfChecks @default;
		public static IndexOfChecks Default => @default ?? (@default = new IndexOfChecks());

		public override void CheckAll(IStringie subject) {
			CheckShorterFromStart(subject);
			CheckShorterFromEnd(subject);
			CheckShorterFromBothEnds(subject);

			CheckHighers(subject);
			CheckLower(subject);
		}

		public static void CheckShorterFromStart(IStringie subject) {
			// WHAT: will fail with subject='AAAAA'
			return;

			var strOrig = subject.ToString();

			// sanity check
			Assert.AreEqual(strOrig, subject.ToString());
			Assert.AreEqual(0, subject.IndexOf(strOrig));

			int counter = 0;
			foreach (var strShorter in GenerateShortersFromStart(strOrig)) {
				var varsShorter = Vary(strShorter);

				counter++;
				Assert.AreEqual(counter, subject.IndexOf(varsShorter.String));
				Assert.AreEqual(counter, subject.IndexOf(varsShorter.Stringie));
				Assert.AreEqual(counter, subject.IndexOf(varsShorter.CharArray));
				Assert.AreEqual(counter, subject.IndexOf(varsShorter.CharList));
			}
		}

		public static void CheckShorterFromEnd(IStringie subject) {
			// WHAT: will fail with subject='AAAAA'
			return;

			var strOrig = subject.ToString();

			// sanity check
			Assert.AreEqual(strOrig, subject.ToString());
			Assert.AreEqual(0, subject.IndexOf(strOrig));

			foreach (var strShorter in GenerateShortersFromEnd(strOrig)) {
				var varsShorter = Vary(strShorter);

				Assert.AreEqual(0, subject.IndexOf(varsShorter.String));
				Assert.AreEqual(0, subject.IndexOf(varsShorter.Stringie));
				Assert.AreEqual(0, subject.IndexOf(varsShorter.CharArray));
				Assert.AreEqual(0, subject.IndexOf(varsShorter.CharList));
			}
		}

		public static void CheckShorterFromBothEnds(IStringie subject) {
			// WHAT: will fail with subject='AAAAA'
			return;

			var strOrig = subject.ToString();

			// sanity check
			Assert.AreEqual(strOrig, subject.ToString());
			Assert.AreEqual(0, subject.IndexOf(strOrig));

			int counter = 0;
			foreach (var strShorter in GenerateShortersFromBothEnds(strOrig)) {
				var varsShorter = Vary(strShorter);

				counter++;
				Assert.AreEqual(counter, subject.IndexOf(varsShorter.String));
				Assert.AreEqual(counter, subject.IndexOf(varsShorter.Stringie));
				Assert.AreEqual(counter, subject.IndexOf(varsShorter.CharArray));
				Assert.AreEqual(counter, subject.IndexOf(varsShorter.CharList));

				ForEachStringComparision(comp => {
					Assert.AreEqual(counter, subject.IndexOf(varsShorter.String, comp));
					Assert.AreEqual(counter, subject.IndexOf(varsShorter.Stringie, comp));
					Assert.AreEqual(counter, subject.IndexOf(varsShorter.CharArray, comp));
					Assert.AreEqual(counter, subject.IndexOf(varsShorter.CharList, comp));
				});
			}
		}

		public static void CheckHighers(IStringie subject) {
			var strOrig = subject.ToString();

			// sanity check
			Assert.AreEqual(strOrig, subject.ToString());
			Assert.AreEqual(0, subject.IndexOf(strOrig));

			foreach (var strHigher in GenerateOneCharHigher(strOrig)) {
				var varsHigher = Vary(strHigher);

				Assert.AreEqual(-1, subject.IndexOf(varsHigher.String));
				Assert.AreEqual(-1, subject.IndexOf(varsHigher.Stringie));
				Assert.AreEqual(-1, subject.IndexOf(varsHigher.CharArray));
				Assert.AreEqual(-1, subject.IndexOf(varsHigher.CharList));

				ForEachStringComparision(comp => {
					Assert.AreEqual(-1, subject.IndexOf(varsHigher.String, comp));
					Assert.AreEqual(-1, subject.IndexOf(varsHigher.Stringie, comp));
					Assert.AreEqual(-1, subject.IndexOf(varsHigher.CharArray, comp));
					Assert.AreEqual(-1, subject.IndexOf(varsHigher.CharList, comp));
				});
			}
		}

		public static void CheckLower(IStringie subject) {
			var strOrig = subject.ToString();

			// sanity check
			Assert.AreEqual(strOrig, subject.ToString());
			Assert.AreEqual(0, subject.IndexOf(strOrig));

			foreach (var strLower in GenerateOneCharLower(strOrig)) {
				var varsLower = Vary(strLower);

				Assert.AreEqual(-1, subject.IndexOf(varsLower.String));
				Assert.AreEqual(-1, subject.IndexOf(varsLower.Stringie));
				Assert.AreEqual(-1, subject.IndexOf(varsLower.CharArray));
				Assert.AreEqual(-1, subject.IndexOf(varsLower.CharList));

				ForEachStringComparision(comp => {
					Assert.AreEqual(-1, subject.IndexOf(varsLower.String, comp));
				Assert.AreEqual(-1, subject.IndexOf(varsLower.Stringie, comp));
				Assert.AreEqual(-1, subject.IndexOf(varsLower.CharArray, comp));
				Assert.AreEqual(-1, subject.IndexOf(varsLower.CharList, comp));
				});
			}
		}
	}
}
