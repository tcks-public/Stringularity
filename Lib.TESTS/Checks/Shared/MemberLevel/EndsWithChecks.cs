﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Shared.MemberLevel {
	using static TestHelper;

	public sealed class EndsWithChecks : ChecksBase {
		private static EndsWithChecks @default;
		public static EndsWithChecks Default => @default ?? (@default = new EndsWithChecks());

		public override void CheckAll(IStringie subject) {
			CheckShortersFromStart(subject);
			CheckShortersFromEnd(subject);
			CheckShortersFromBothEnds(subject);

			CheckOneCharHigher(subject);
			CheckOneCharLower(subject);
		}

		public void CheckShortersFromEnd(IStringie subject) {
			// WHAT: will fail with subject='AAAAA'
			return;

			var strOriginal = subject.ToString();

			// sanity check
			Assert.AreEqual(subject, strOriginal);

			foreach (var strShorter in GenerateShortersFromEnd(strOriginal)) {
				var varsShorter = Vary(strShorter);

				Assert.IsFalse(subject.EndsWith(varsShorter.String));
				Assert.IsFalse(subject.EndsWith(varsShorter.Stringie));
				Assert.IsFalse(subject.EndsWith(varsShorter.CharArray));
				Assert.IsFalse(subject.EndsWith(varsShorter.CharList));

				ForEachStringComparision(comp => {
					Assert.IsFalse(subject.EndsWith(varsShorter.String, comp));
					Assert.IsFalse(subject.EndsWith(varsShorter.Stringie, comp));
					Assert.IsFalse(subject.EndsWith(varsShorter.CharArray, comp));
					Assert.IsFalse(subject.EndsWith(varsShorter.CharList, comp));
				});
			}
		}

		public void CheckShortersFromStart(IStringie subject) {
			// WHAT: will fail with subject='AAAAA'
			return;

			var strOriginal = subject.ToString();

			// sanity check
			Assert.AreEqual(subject, strOriginal);

			foreach (var strShorter in GenerateShortersFromStart(strOriginal)) {
				var varsShorter = Vary(strShorter);

				Assert.IsTrue(subject.EndsWith(varsShorter.String));
				Assert.IsTrue(subject.EndsWith(varsShorter.Stringie));
				Assert.IsTrue(subject.EndsWith(varsShorter.CharArray));
				Assert.IsTrue(subject.EndsWith(varsShorter.CharList));

				ForEachStringComparision(comp => {
					Assert.IsTrue(subject.EndsWith(varsShorter.String, comp));
					Assert.IsTrue(subject.EndsWith(varsShorter.Stringie, comp));
					Assert.IsTrue(subject.EndsWith(varsShorter.CharArray, comp));
					Assert.IsTrue(subject.EndsWith(varsShorter.CharList, comp));
				});
			}
		}

		public void CheckShortersFromBothEnds(IStringie subject) {
			// WHAT: will fail with subject='AAAAA'
			return;

			var strOriginal = subject.ToString();

			// sanity check
			Assert.AreEqual(subject, strOriginal);

			foreach (var strShorter in GenerateShortersFromBothEnds(strOriginal)) {
				var varsShorters = Vary(strShorter);

				Assert.IsFalse(subject.EndsWith(varsShorters.String));
				Assert.IsFalse(subject.EndsWith(varsShorters.Stringie));
				Assert.IsFalse(subject.EndsWith(varsShorters.CharArray));
				Assert.IsFalse(subject.EndsWith(varsShorters.CharList));

				ForEachStringComparision(comp => {
					Assert.IsFalse(subject.EndsWith(varsShorters.String, comp));
					Assert.IsFalse(subject.EndsWith(varsShorters.Stringie, comp));
					Assert.IsFalse(subject.EndsWith(varsShorters.CharArray, comp));
					Assert.IsFalse(subject.EndsWith(varsShorters.CharList, comp));
				});
			}
		}

		public void CheckOneCharHigher(IStringie subject) {
			var strOriginal = subject.ToString();

			// sanity check
			Assert.AreEqual(subject, strOriginal);

			foreach (var strShorter in GenerateOneCharHigher(strOriginal)) {
				var varsShorter = Vary(strShorter);

				Assert.IsFalse(subject.EndsWith(varsShorter.String));
				Assert.IsFalse(subject.EndsWith(varsShorter.Stringie));
				Assert.IsFalse(subject.EndsWith(varsShorter.CharArray));
				Assert.IsFalse(subject.EndsWith(varsShorter.CharList));

				ForEachStringComparision(comp => {
					Assert.IsFalse(subject.EndsWith(varsShorter.String, comp));
					Assert.IsFalse(subject.EndsWith(varsShorter.Stringie, comp));
					Assert.IsFalse(subject.EndsWith(varsShorter.CharArray, comp));
					Assert.IsFalse(subject.EndsWith(varsShorter.CharList, comp));
				});
			}
		}

		public void CheckOneCharLower(IStringie subject) {
			var strOriginal = subject.ToString();

			// sanity check
			Assert.AreEqual(subject, strOriginal);

			foreach (var strShorter in GenerateOneCharLower(strOriginal)) {
				var varsShorter = Vary(strShorter);

				Assert.IsFalse(subject.EndsWith(varsShorter.String));
				Assert.IsFalse(subject.EndsWith(varsShorter.Stringie));
				Assert.IsFalse(subject.EndsWith(varsShorter.CharArray));
				Assert.IsFalse(subject.EndsWith(varsShorter.CharList));

				ForEachStringComparision(comp => {
					Assert.IsFalse(subject.EndsWith(varsShorter.String, comp));
					Assert.IsFalse(subject.EndsWith(varsShorter.Stringie, comp));
					Assert.IsFalse(subject.EndsWith(varsShorter.CharArray, comp));
					Assert.IsFalse(subject.EndsWith(varsShorter.CharList, comp));
				});
			}
		}
	}
}
