﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity.Checks.Shared.MemberLevel {
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using static TestHelper;

	public sealed class ContainsChecks : ChecksBase {
		private static ContainsChecks @default;
		public static ContainsChecks Default => @default ?? (@default = new ContainsChecks());

		public override void CheckAll(IStringie subject) {
			CheckShorterFromStart(subject);
			CheckShorterFromEnd(subject);
			CheckShorterFromBothEnds(subject);
			CheckLonger(subject);
		}

		public void CheckShorterFromStart(IStringie subject) {
			var strOrig = subject.ToString();

			foreach (var strShorter in GenerateShortersFromStart(strOrig)) {
				var varsShorter = Vary(strShorter);

				Assert.IsTrue(subject.Contains(varsShorter.String));
				Assert.IsTrue(subject.Contains(varsShorter.Stringie));
				Assert.IsTrue(subject.Contains(varsShorter.CharArray));
				Assert.IsTrue(subject.Contains(varsShorter.CharList));

				ForEachStringComparision(comp => {
					Assert.IsTrue(subject.Contains(varsShorter.String, comp));
					Assert.IsTrue(subject.Contains(varsShorter.Stringie, comp));
					Assert.IsTrue(subject.Contains(varsShorter.CharArray, comp));
					Assert.IsTrue(subject.Contains(varsShorter.CharList, comp));
				});
			}
		}

		public void CheckShorterFromEnd(IStringie subject) {
			var strOrig = subject.ToString();
			var shorters = GenerateShortersFromEnd(strOrig);
			if (shorters is null) {
				// can not be shorter => skip
				return;
			}

			foreach (var strShorter in shorters) {
				var varsShorter = Vary(strShorter);

				Assert.IsTrue(subject.Contains(varsShorter.String));
				Assert.IsTrue(subject.Contains(varsShorter.Stringie));
				Assert.IsTrue(subject.Contains(varsShorter.CharArray));
				Assert.IsTrue(subject.Contains(varsShorter.CharList));

				ForEachStringComparision(comp => {
					Assert.IsTrue(subject.Contains(varsShorter.String, comp));
					Assert.IsTrue(subject.Contains(varsShorter.Stringie, comp));
					Assert.IsTrue(subject.Contains(varsShorter.CharArray, comp));
					Assert.IsTrue(subject.Contains(varsShorter.CharList, comp));
				});
			}
		}

		public void CheckShorterFromBothEnds(IStringie subject) {
			var strOrig = subject.ToString();

			foreach (var strShorter in GenerateShortersFromEnd(strOrig)) {
				var varsShorter = Vary(strShorter);

				Assert.IsTrue(subject.Contains(varsShorter.String));
				Assert.IsTrue(subject.Contains(varsShorter.Stringie));
				Assert.IsTrue(subject.Contains(varsShorter.CharArray));
				Assert.IsTrue(subject.Contains(varsShorter.CharList));

				ForEachStringComparision(comp => {
					Assert.IsTrue(subject.Contains(varsShorter.String, comp));
					Assert.IsTrue(subject.Contains(varsShorter.Stringie, comp));
					Assert.IsTrue(subject.Contains(varsShorter.CharArray, comp));
					Assert.IsTrue(subject.Contains(varsShorter.CharList, comp));
				});
			}
		}

		public void CheckLonger(IStringie subject) {
			var strOrig = subject.ToString();
			var longers = GenerateAppends(strOrig, 10);
			if (longers is null) {
				// can not be shorter => skip
				return;
			}

			var varsOrig = Vary(strOrig);

			foreach (var strLonger in longers) {
				var varsLonger = Vary(strLonger);

				Assert.IsFalse(subject.Contains(varsLonger.String));
				Assert.IsFalse(subject.Contains(varsLonger.Stringie));
				Assert.IsFalse(subject.Contains(varsLonger.CharArray));
				Assert.IsFalse(subject.Contains(varsLonger.CharList));

				Assert.IsTrue(varsLonger.Stringie.Contains(varsOrig.String));
				Assert.IsTrue(varsLonger.Stringie.Contains(varsOrig.Stringie));
				Assert.IsTrue(varsLonger.Stringie.Contains(varsOrig.CharArray));
				Assert.IsTrue(varsLonger.Stringie.Contains(varsOrig.CharList));

				ForEachStringComparision(comp => {
					Assert.IsFalse(subject.Contains(varsLonger.String, comp));
					Assert.IsFalse(subject.Contains(varsLonger.Stringie, comp));
					Assert.IsFalse(subject.Contains(varsLonger.CharArray, comp));
					Assert.IsFalse(subject.Contains(varsLonger.CharList, comp));

					Assert.IsTrue(varsLonger.Stringie.Contains(varsOrig.String, comp));
					Assert.IsTrue(varsLonger.Stringie.Contains(varsOrig.Stringie, comp));
					Assert.IsTrue(varsLonger.Stringie.Contains(varsOrig.CharArray, comp));
					Assert.IsTrue(varsLonger.Stringie.Contains(varsOrig.CharList, comp));
				});
			}
		}
	}
}
