﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Shared.MemberLevel {
	using static TestHelper;

	public sealed class CloneChecks : ChecksBase {
		private static CloneChecks @default;
		public static CloneChecks Default => @default ?? (@default = new CloneChecks());

		public override void CheckAll(IStringie subject) {
			if (subject is null) { throw new ArgumentNullException(nameof(subject)); }

			var clone = subject.Clone();
			Assert.AreNotSame(subject, clone);

			AssertEquality(subject, clone);
		}
	}
}
