﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Shared.MemberLevel {
	using static TestHelper;

	public sealed class AppendChecks : ChecksBase {
		private static AppendChecks @default;
		public static AppendChecks Default => @default ?? (@default = new AppendChecks());

		public override void CheckAll(IStringie subject) {
			CheckWithNull(subject);
			CheckWithEmpty(subject);
			CheckWithOneChar(subject);
			CheckWithWords(subject);
		}

		public void CheckWithNull(IStringie subject) {
			Assert.AreSame(subject, subject.Append((char[])null));
			Assert.AreSame(subject, subject.Append((IReadOnlyList<char>)null));
			Assert.AreSame(subject, subject.Append((string)null));
			Assert.AreSame(subject, subject.Append((IStringie)null));
		}

		public void CheckWithEmpty(IStringie subject) {
			var empty = Vary(string.Empty);

			Assert.AreSame(subject, subject.Append(empty.String));
			Assert.AreSame(subject, subject.Append(empty.Stringie));
			Assert.AreSame(subject, subject.Append(empty.CharArray));
			Assert.AreSame(subject, subject.Append(empty.CharList));
		}

		[TestMethod]
		public void CheckWithOneChar(IStringie subject) {
			var letter = Vary("A");

			var strExpected = subject.ToString() + "A";
			var expected = Stringie.Create(strExpected);

			Assert.AreEqual(strExpected, subject.Append('A').ToString());
			Assert.AreEqual(strExpected, subject.Append(letter.String).ToString());
			Assert.AreEqual(strExpected, subject.Append(letter.Stringie).ToString());
			Assert.AreEqual(strExpected, subject.Append(letter.CharArray).ToString());
			Assert.AreEqual(strExpected, subject.Append(letter.CharList).ToString());

			AssertEquality(expected, subject.Append('A'));
			AssertEquality(expected, subject.Append(letter.String));
			AssertEquality(expected, subject.Append(letter.Stringie));
			AssertEquality(expected, subject.Append(letter.CharArray));
			AssertEquality(expected, subject.Append(letter.CharList));
		}

		[TestMethod]
		public void CheckWithWords(IStringie subject) {
			var word = Vary("My word");

			var strExpected = subject.ToString() + "My word";
			var expected = Stringie.Create(strExpected);

			Assert.AreEqual(strExpected, subject.Append(word.String).ToString());
			Assert.AreEqual(strExpected, subject.Append(word.Stringie).ToString());
			Assert.AreEqual(strExpected, subject.Append(word.CharArray).ToString());
			Assert.AreEqual(strExpected, subject.Append(word.CharList).ToString());
			
			AssertEquality(expected, subject.Append(word.String));
			AssertEquality(expected, subject.Append(word.Stringie));
			AssertEquality(expected, subject.Append(word.CharArray));
			AssertEquality(expected, subject.Append(word.CharList));
		}
	}
}
