﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Shared.MemberLevel {
	using static TestHelper;

	public sealed class PadLeftChecks : ChecksBase {
		private static PadLeftChecks @default;
		public static PadLeftChecks Default => @default ?? (@default = new PadLeftChecks());

		public override void CheckAll(IStringie subject) {
			CheckLowerPadding(subject);
			CheckExactPadding(subject);
			CheckLongerPadding(subject);
		}

		public void CheckLowerPadding(IStringie subject) {
			var width = subject.Length - 1;
			if (width < 0) { return; }

			Assert.AreSame(subject, subject.PadLeft('A', width));
		}

		public void CheckExactPadding(IStringie subject) {
			Assert.AreSame(subject, subject.PadLeft('A', subject.Length));
		}

		public void CheckLongerPadding(IStringie subject) {
			for (var i = 0; i < 10; i++) {
				var padding = (i + 1) * 7;
				var width = subject.Length + padding;

				var result = subject.PadLeft('A', width);
				Assert.AreEqual(width, result.Length);
				Assert.AreNotSame(result, subject);
				Assert.IsTrue(result.EndsWith(subject));

				for (var k = 0; k < padding; k++) {
					Assert.AreEqual('A', result[k]);
				}
			}
		}
	}
}
