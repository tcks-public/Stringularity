﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Shared.MemberLevel {
	using static TestHelper;

	public sealed class PadRightChecks : ChecksBase {
		private static PadRightChecks @default;
		public static PadRightChecks Default => @default ?? (@default = new PadRightChecks());

		public override void CheckAll(IStringie subject) {
			CheckLowerPadding(subject);
			CheckExactPadding(subject);
			CheckLongerPadding(subject);
		}

		public void CheckLowerPadding(IStringie subject) {
			var width = subject.Length - 1;
			if (width < 0) { return; }

			Assert.AreSame(subject, subject.PadRight('A', width));
		}

		public void CheckExactPadding(IStringie subject) {
			Assert.AreSame(subject, subject.PadRight('A', subject.Length));
		}

		public void CheckLongerPadding(IStringie subject) {
			for (var i = 0; i < 10; i++) {
				var padding = (i + 1) * 7;
				var width = subject.Length + padding;

				var result = subject.PadRight('A', width);
				Assert.AreEqual(width, result.Length);
				Assert.AreNotSame(result, subject);
				Assert.IsTrue(result.StartsWith(subject));

				for (var k = 0; k < padding; k++) {
					Assert.AreEqual('A', result[subject.Length + k]);
				}
			}
		}
	}
}
