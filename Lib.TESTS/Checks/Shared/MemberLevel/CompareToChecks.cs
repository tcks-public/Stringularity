﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Shared.MemberLevel {
	using static TestHelper;

	public sealed class CompareToChecks : ChecksBase {
		private static CompareToChecks @default;
		public static CompareToChecks Default => @default ?? (@default = new CompareToChecks());

		public override void CheckAll(IStringie subject) {
			CheckNull(subject);
			CheckEmpty(subject);
			CheckShorter(subject);
			CheckLonger(subject);
			CheckModifiedToHigher(subject);
			CheckModifiedToLower(subject);
		}

		public void CheckNull(IStringie subject) {
			var varsNull = Vary(null);

			Assert.AreEqual(1, subject.CompareTo(varsNull.String));
			Assert.AreEqual(1, subject.CompareTo(varsNull.Stringie));
			Assert.AreEqual(1, subject.CompareTo(varsNull.CharArray));
			Assert.AreEqual(1, subject.CompareTo(varsNull.CharList));
		}

		public void CheckEmpty(IStringie subject) {
			var varsEmpty = Vary(string.Empty);

			if (subject.IsEmpty) {
				Assert.AreEqual(0, subject.CompareTo(varsEmpty.String));
				Assert.AreEqual(0, subject.CompareTo(varsEmpty.Stringie));
				Assert.AreEqual(0, subject.CompareTo(varsEmpty.CharArray));
				Assert.AreEqual(0, subject.CompareTo(varsEmpty.CharList));
			}
			else {
				Assert.AreEqual(1, subject.CompareTo(varsEmpty.String));
				Assert.AreEqual(1, subject.CompareTo(varsEmpty.Stringie));
				Assert.AreEqual(1, subject.CompareTo(varsEmpty.CharArray));
				Assert.AreEqual(1, subject.CompareTo(varsEmpty.CharList));
			}
		}

		public void CheckShorter(IStringie subject) {
			var strOrig = subject.ToString();

			// sanity checks
			Assert.AreEqual(subject, strOrig);
			Assert.AreEqual(0, subject.CompareTo(strOrig));

			if (strOrig.Length < 1) {
				// can not compare to shorter string => skip
				return;
			}

			while (strOrig.Length > 0) {
				var strShort = strOrig.Substring(0, strOrig.Length - 1);
				var varsShort = Vary(strShort);

				Assert.AreEqual(1, subject.CompareTo(varsShort.String));
				Assert.AreEqual(1, subject.CompareTo(varsShort.Stringie));
				Assert.AreEqual(1, subject.CompareTo(varsShort.CharArray));
				Assert.AreEqual(1, subject.CompareTo(varsShort.CharList));

				// will try compare to even shorter string
				strOrig = strShort;
			}
		}

		public void CheckLonger(IStringie subject) {
			var strOrig = subject.ToString();

			// sanity checks
			Assert.AreEqual(subject, strOrig);
			Assert.AreEqual(0, subject.CompareTo(strOrig));

			var attempts = 10;
			for (var i = 0; i < attempts; i++) {
				var strLong = strOrig + (char)i;
				var varsLong = Vary(strLong);

				Assert.AreEqual(-1, subject.CompareTo(varsLong.String));
				Assert.AreEqual(-1, subject.CompareTo(varsLong.Stringie));
				Assert.AreEqual(-1, subject.CompareTo(varsLong.CharArray));
				Assert.AreEqual(-1, subject.CompareTo(varsLong.CharList));

				// will try compare to event longer string
				strOrig = strLong;
			}
		}

		public void CheckModifiedToHigher(IStringie subject) {
			var strOrig = subject.ToString();

			// sanity checks
			Assert.AreEqual(subject, strOrig);
			Assert.AreEqual(0, subject.CompareTo(strOrig));

			var arrOrig = strOrig.ToCharArray();
			for (var i = 0; i < strOrig.Length; i++) {
				var arrHigher = (char[])arrOrig.Clone();
				arrHigher[i] = (char)(arrHigher[i] + 1);
				var strHigher = new string(arrHigher);

				var varsHigher = Vary(strHigher);

				Assert.AreEqual(-1, subject.CompareTo(varsHigher.String));
				Assert.AreEqual(-1, subject.CompareTo(varsHigher.Stringie));
				Assert.AreEqual(-1, subject.CompareTo(varsHigher.CharArray));
				Assert.AreEqual(-1, subject.CompareTo(varsHigher.CharList));
			}
		}

		public void CheckModifiedToLower(IStringie subject) {
			var strOrig = subject.ToString();

			// sanity checks
			Assert.AreEqual(subject, strOrig);
			Assert.AreEqual(0, subject.CompareTo(strOrig));

			var arrOrig = strOrig.ToCharArray();
			for (var i = 0; i < strOrig.Length; i++) {
				var arrLower = (char[])arrOrig.Clone();
				arrLower[i] = (char)(arrLower[i] - 1);
				var strLower = new string(arrLower);

				var varsLower = Vary(strLower);

				Assert.AreEqual(1, subject.CompareTo(varsLower.String));
				Assert.AreEqual(1, subject.CompareTo(varsLower.Stringie));
				Assert.AreEqual(1, subject.CompareTo(varsLower.CharArray));
				Assert.AreEqual(1, subject.CompareTo(varsLower.CharList));
			}
		}
	}
}
