﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Shared.MemberLevel {
	using static TestHelper;

	public sealed class PrependChecks : ChecksBase {
		private static PrependChecks @default;
		public static PrependChecks Default => @default ?? (@default = new PrependChecks());

		public override void CheckAll(IStringie subject) {
			CheckWithNull(subject);
			CheckWithEmpty(subject);
			CheckWithOneChar(subject);
			CheckWithWords(subject);
		}

		public void CheckWithNull(IStringie subject) {
			Assert.AreSame(subject, subject.Append((char[])null));
			Assert.AreSame(subject, subject.Append((IReadOnlyList<char>)null));
			Assert.AreSame(subject, subject.Append((string)null));
			Assert.AreSame(subject, subject.Append((IStringie)null));
		}

		public void CheckWithEmpty(IStringie subject) {
			var empty = Vary(string.Empty);

			Assert.AreSame(subject, subject.Append(empty.String));
			Assert.AreSame(subject, subject.Append(empty.Stringie));
			Assert.AreSame(subject, subject.Append(empty.CharArray));
			Assert.AreSame(subject, subject.Append(empty.CharList));
		}

		[TestMethod]
		public void CheckWithOneChar(IStringie subject) {
			var letter = Vary("A");

			var strExpected = "A" + subject.ToString();
			var expected = Stringie.Create(strExpected);

			Assert.AreEqual(strExpected, subject.Prepend('A').ToString());
			Assert.AreEqual(strExpected, subject.Prepend(letter.String).ToString());
			Assert.AreEqual(strExpected, subject.Prepend(letter.Stringie).ToString());
			Assert.AreEqual(strExpected, subject.Prepend(letter.CharArray).ToString());
			Assert.AreEqual(strExpected, subject.Prepend(letter.CharList).ToString());

			AssertEquality(expected, subject.Prepend('A'));
			AssertEquality(expected, subject.Prepend(letter.String));
			AssertEquality(expected, subject.Prepend(letter.Stringie));
			AssertEquality(expected, subject.Prepend(letter.CharArray));
			AssertEquality(expected, subject.Prepend(letter.CharList));
		}

		[TestMethod]
		public void CheckWithWords(IStringie subject) {
			var word = Vary("My word");

			var strExpected = "My word" + subject.ToString();
			var expected = Stringie.Create(strExpected);

			Assert.AreEqual(strExpected, subject.Prepend(word.String).ToString());
			Assert.AreEqual(strExpected, subject.Prepend(word.Stringie).ToString());
			Assert.AreEqual(strExpected, subject.Prepend(word.CharArray).ToString());
			Assert.AreEqual(strExpected, subject.Prepend(word.CharList).ToString());
			
			AssertEquality(expected, subject.Prepend(word.String));
			AssertEquality(expected, subject.Prepend(word.Stringie));
			AssertEquality(expected, subject.Prepend(word.CharArray));
			AssertEquality(expected, subject.Prepend(word.CharList));
		}
	}
}
