﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Shared {
	using static TestHelper;

	public sealed class SelfConsistencyChecks : ChecksBase {
		private static SelfConsistencyChecks @default;
		public static SelfConsistencyChecks Default => @default ?? (@default = new SelfConsistencyChecks());

		public override void CheckAll(IStringie subject) {
			if (subject is null) { throw new ArgumentNullException(nameof(subject)); }

			Assert.IsTrue(subject.Length >= 0);
			Assert.AreEqual(subject.IsEmpty, subject.Length == 0);

			var sb = new StringBuilder();
			subject.ToString(sb);
			Assert.AreEqual(0, StringComparer.InvariantCulture.Compare(sb.ToString(), subject.ToString()));

			Assert.AreEqual(subject.ToString(), subject.ToString());

			#region CompareTo
			Assert.AreEqual(0, subject.CompareTo(subject));
			Assert.AreEqual(0, subject.CompareTo(subject.ToString()));
			Assert.AreEqual(0, subject.CompareTo(subject.ToString().ToCharArray()));
			Assert.AreEqual(0, subject.CompareTo(subject.ToString().ToCharArray().ToList()));
			#endregion CompareTo

			#region Clone
			{
				var clones = new[] { subject.Clone(), subject.Clone(), subject.Clone().Clone() };
				for (var i = 0; i < clones.Length; i++) {
					Assert.AreNotSame(subject, clones[i]);
					Assert.AreEqual(subject, clones[i]);
					for (var k = i - 1; k >= 0; k--) {
						Assert.AreNotSame(clones[i], clones[k]);
						Assert.AreEqual(clones[i], clones[k]);
					}
				}
			}
			#endregion Clone

			#region Contains
			Assert.IsTrue(subject.Contains(subject));
			Assert.IsTrue(subject.Contains(subject.ToString()));
			Assert.IsTrue(subject.Contains(subject.ToString().ToCharArray()));
			Assert.IsTrue(subject.Contains(subject.ToString().ToCharArray().ToList()));

			ForEachStringComparision(comp => {
				Assert.IsTrue(subject.Contains(subject, comp));
				Assert.IsTrue(subject.Contains(subject.ToString(), comp));
				Assert.IsTrue(subject.Contains(subject.ToString().ToCharArray(), comp));
				Assert.IsTrue(subject.Contains(subject.ToString().ToCharArray().ToList(), comp));
			});
			#endregion Contains

			#region EndsWith
			Assert.IsTrue(subject.EndsWith(subject));
			Assert.IsTrue(subject.EndsWith(subject.ToString()));
			Assert.IsTrue(subject.EndsWith(subject.ToString().ToCharArray()));
			Assert.IsTrue(subject.EndsWith(subject.ToString().ToCharArray().ToList()));

			ForEachStringComparision(comp => {
				Assert.IsTrue(subject.EndsWith(subject, comp));
				Assert.IsTrue(subject.EndsWith(subject.ToString(), comp));
				Assert.IsTrue(subject.EndsWith(subject.ToString().ToCharArray(), comp));
				Assert.IsTrue(subject.EndsWith(subject.ToString().ToCharArray().ToList(), comp));
			});
			#endregion EndsWith

			#region Equals
			Assert.IsTrue(subject.Equals(subject));
			Assert.IsTrue(subject.Equals((object)subject));

			Assert.IsTrue(subject.Equals(subject.ToString()));
			Assert.IsTrue(subject.Equals((object)subject.ToString()));

			Assert.IsTrue(subject.Equals(subject.ToString().ToCharArray()));
			Assert.IsTrue(subject.Equals((object)subject.ToString().ToCharArray()));

			Assert.IsTrue(subject.Equals(subject.ToString().ToCharArray().ToList()));
			Assert.IsTrue(subject.Equals((object)subject.ToString().ToCharArray().ToList()));
			#endregion Equals

			#region Flatten
			Assert.AreEqual(subject, subject.Flatten());
			Assert.AreEqual(subject.Length, subject.Flatten().Length);
			Assert.AreEqual(subject.IsEmpty, subject.Flatten().IsEmpty);
			Assert.AreEqual(0, StringComparer.InvariantCulture.Compare(subject.ToString(), subject.Flatten().ToString()));

			Assert.AreEqual(subject.Flatten(), subject.Flatten());
			Assert.AreEqual(subject.Flatten().Length, subject.Flatten().Length);
			Assert.AreEqual(subject.Flatten().IsEmpty, subject.Flatten().IsEmpty);
			Assert.AreEqual(0, StringComparer.InvariantCulture.Compare(subject.Flatten().ToString(), subject.Flatten().ToString()));
			#endregion Flatten

			#region GetHashCode
			Assert.AreEqual(subject.GetHashCode(), subject.GetHashCode());
			Assert.AreEqual(subject.GetHashCode(), subject.Clone().GetHashCode());
			Assert.AreEqual(subject.Clone().GetHashCode(), subject.Clone().GetHashCode());

			Assert.AreEqual(subject.GetHashCode(), subject.Flatten().GetHashCode());
			Assert.AreEqual(subject.Flatten().GetHashCode(), subject.Flatten().GetHashCode());
			Assert.AreEqual(subject.Flatten().GetHashCode(), subject.Clone().Flatten().GetHashCode());
			#endregion GetHashCode

			#region IndexOf
			Assert.AreEqual(0, subject.IndexOf(subject));
			Assert.AreEqual(0, subject.IndexOf(subject.ToString()));
			Assert.AreEqual(0, subject.IndexOf(subject.ToString().ToCharArray()));
			Assert.AreEqual(0, subject.IndexOf(subject.ToString().ToCharArray().ToList()));

			ForEachStringComparision(comp => {
				Assert.AreEqual(0, subject.IndexOf(subject, comp));
				Assert.AreEqual(0, subject.IndexOf(subject.ToString(), comp));
				Assert.AreEqual(0, subject.IndexOf(subject.ToString().ToCharArray(), comp));
				Assert.AreEqual(0, subject.IndexOf(subject.ToString().ToCharArray().ToList(), comp));
			});
			#endregion IndexOf

			#region StartsWith
			Assert.IsTrue(subject.StartsWith(subject));
			Assert.IsTrue(subject.StartsWith(subject.ToString()));
			Assert.IsTrue(subject.StartsWith(subject.ToString().ToCharArray()));
			Assert.IsTrue(subject.StartsWith(subject.ToString().ToCharArray().ToList()));

			ForEachStringComparision(comp => {
				Assert.IsTrue(subject.StartsWith(subject, comp));
				Assert.IsTrue(subject.StartsWith(subject.ToString(), comp));
				Assert.IsTrue(subject.StartsWith(subject.ToString().ToCharArray(), comp));
				Assert.IsTrue(subject.StartsWith(subject.ToString().ToCharArray().ToList(), comp));
			});
			#endregion StartsWith
		}
	}
}
