﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity.Checks.Shared {
	using static TestHelper;

	public sealed class NullConsistencyChecks : ChecksBase {
		private static NullConsistencyChecks @default;
		public static NullConsistencyChecks Default => @default ?? (@default = new NullConsistencyChecks());

		public override void CheckAll(IStringie subject) {
			if (subject is null) { throw new ArgumentNullException(nameof(subject)); }

			var vars = Vary(null);

			#region CompareTo
			Assert.AreEqual(subject.CompareTo(vars.String), subject.CompareTo(vars.String));
			Assert.AreEqual(subject.CompareTo(vars.Stringie), subject.CompareTo(vars.Stringie));
			Assert.AreEqual(subject.CompareTo(vars.CharArray), subject.CompareTo(vars.CharArray));
			Assert.AreEqual(subject.CompareTo(vars.CharList), subject.CompareTo(vars.CharList));
			#endregion CompareTo

			#region Contains
			Assert.IsFalse(subject.Contains(vars.String));
			Assert.IsFalse(subject.Contains(vars.Stringie));
			Assert.IsFalse(subject.Contains(vars.CharArray));
			Assert.IsFalse(subject.Contains(vars.CharList));

			ForEachStringComparision(comp => {
				Assert.IsFalse(subject.Contains(vars.String, comp));
				Assert.IsFalse(subject.Contains(vars.Stringie, comp));
				Assert.IsFalse(subject.Contains(vars.CharArray, comp));
				Assert.IsFalse(subject.Contains(vars.CharList, comp));
			});
			#endregion Contains

			#region EndsWith
			Assert.IsFalse(subject.EndsWith(vars.String));
			Assert.IsFalse(subject.EndsWith(vars.Stringie));
			Assert.IsFalse(subject.EndsWith(vars.CharArray));
			Assert.IsFalse(subject.EndsWith(vars.CharList));

			ForEachStringComparision(comp => {
				Assert.IsFalse(subject.EndsWith(vars.String, comp));
				Assert.IsFalse(subject.EndsWith(vars.Stringie, comp));
				Assert.IsFalse(subject.EndsWith(vars.CharArray, comp));
				Assert.IsFalse(subject.EndsWith(vars.CharList, comp));
			});
			#endregion EndsWith

			#region Equals
			Assert.IsFalse(subject.Equals(vars.String));
			Assert.IsFalse(subject.Equals((object)vars.String));
			Assert.IsFalse(subject.Equals(vars.Stringie));
			Assert.IsFalse(subject.Equals((object)vars.Stringie));
			Assert.IsFalse(subject.Equals(vars.CharArray));
			Assert.IsFalse(subject.Equals((object)vars.CharArray));
			Assert.IsFalse(subject.Equals(vars.CharList));
			Assert.IsFalse(subject.Equals((object)vars.CharList));
			#endregion Equals

			#region EndsWith
			Assert.IsFalse(subject.EndsWith(vars.String));
			Assert.IsFalse(subject.EndsWith(vars.Stringie));
			Assert.IsFalse(subject.EndsWith(vars.CharArray));
			Assert.IsFalse(subject.EndsWith(vars.CharList));

			ForEachStringComparision(comp => {
				Assert.IsFalse(subject.EndsWith(vars.String, comp));
				Assert.IsFalse(subject.EndsWith(vars.Stringie, comp));
				Assert.IsFalse(subject.EndsWith(vars.CharArray, comp));
				Assert.IsFalse(subject.EndsWith(vars.CharList, comp));
			});
			#endregion EndsWith

			#region IndexOf
			Assert.AreEqual(-1, subject.IndexOf(vars.String));
			Assert.AreEqual(-1, subject.IndexOf(vars.Stringie));
			Assert.AreEqual(-1, subject.IndexOf(vars.CharArray));
			Assert.AreEqual(-1, subject.IndexOf(vars.CharList));

			ForEachStringComparision(comp => {
				Assert.AreEqual(-1, subject.IndexOf(vars.String, comp));
				Assert.AreEqual(-1, subject.IndexOf(vars.Stringie, comp));
				Assert.AreEqual(-1, subject.IndexOf(vars.CharArray, comp));
				Assert.AreEqual(-1, subject.IndexOf(vars.CharList, comp));
			});
			#endregion IndexOf

			#region StartsWith
			Assert.IsFalse(subject.StartsWith(vars.String));
			Assert.IsFalse(subject.StartsWith(vars.Stringie));
			Assert.IsFalse(subject.StartsWith(vars.CharArray));
			Assert.IsFalse(subject.StartsWith(vars.CharList));

			ForEachStringComparision(comp => {
				Assert.IsFalse(subject.StartsWith(vars.String, comp));
				Assert.IsFalse(subject.StartsWith(vars.Stringie, comp));
				Assert.IsFalse(subject.StartsWith(vars.CharArray, comp));
				Assert.IsFalse(subject.StartsWith(vars.CharList, comp));
			});
			#endregion StartsWith
		}
	}
}
