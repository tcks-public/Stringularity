﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Stringularity {
	using Stringularity.Checks.Specialized.Concats;
	using Stringularity.Stringies.Concats;
	using static TestHelper;

	[TestClass]
	public class Concats_Tests {
		[TestMethod]
		public void TwoStringiesWorks() {
			var hello = Stringie.Create("Hello");
			var world = Stringie.Create("World");

			var subject = new ConcatedTwoStringies(hello, world);

			RunWithSharedTests(subject, ConcatedTwoStringiesChecks.Default);

			Assert.AreEqual("HelloWorld", subject.ToString());
		}

		[TestMethod]
		public void StringieArrayWorks() {
			var hello = Stringie.Create("Hello");
			var space = Stringie.Create(" ");
			var world = Stringie.Create("World");
			var excla = Stringie.Create("!");

			var subject = new ConcatedStringieArray(new IStringie[] { hello, space, world, excla });

			RunWithSharedTests(subject, ConcatedStringieArrayChecks.Default);
		}
	}
}
