﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity {
	partial interface IStringie : IEquatable<IStringie> { }
	partial interface IStringie : IEquatable<string> { }
	partial interface IStringie : IEquatable<char> { }
	partial interface IStringie : IEquatable<char[]> { }
	partial interface IStringie : IEquatable<IReadOnlyList<char>> { }
}
