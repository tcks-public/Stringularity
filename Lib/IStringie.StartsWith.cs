﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity {
	partial interface IStringie {
		bool StartsWith(char ch);
		bool StartsWith(char[] chars);
		bool StartsWith(IReadOnlyList<char> chars);
		bool StartsWith(string str);
		bool StartsWith(IStringie str);
		
		bool StartsWith(char ch, StringComparison comparision);
		bool StartsWith(char[] chars, StringComparison comparision);
		bool StartsWith(IReadOnlyList<char> chars, StringComparison comparision);
		bool StartsWith(string str, StringComparison comparision);
		bool StartsWith(IStringie str, StringComparison comparision);
	}
}
