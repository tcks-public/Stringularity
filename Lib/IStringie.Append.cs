﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity {
	partial interface IStringie {
		IStringie Append(char content);
		IStringie Append(char[] content);
		IStringie Append(IReadOnlyList<char> content);
		IStringie Append(string content);
		IStringie Append(IStringie content);
	}
}
