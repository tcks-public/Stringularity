﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity {
	partial interface IStringie {
		int IndexOf(char ch);
		int IndexOf(char[] chars);
		int IndexOf(IReadOnlyList<char> chars);
		int IndexOf(string str);
		int IndexOf(IStringie str);

		int IndexOf(char ch, StringComparison comparision);
		int IndexOf(char[] chars, StringComparison comparision);
		int IndexOf(IReadOnlyList<char> chars, StringComparison comparision);
		int IndexOf(string str, StringComparison comparision);
		int IndexOf(IStringie str, StringComparison comparision);
	}
}
