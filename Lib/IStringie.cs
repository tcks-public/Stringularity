﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity {
	/// <summary>
	/// Interface for types containing string data.
	/// </summary>
	public partial interface IStringie {
		/// <summary>
		/// Returns true if string data is not null and length is zero.
		/// </summary>
		bool IsEmpty { get; }

		/// <summary>
		/// Returns true if string data contains only whitespace characters or length is zero.
		/// </summary>
		bool IsWhitespace { get; }

		/// <summary>
		/// Returns number of characters.
		/// </summary>
		int Length { get; }

		/// <summary>
		/// Returns character on specified position defined by <paramref name="index"/>.
		/// </summary>
		/// <param name="index">Position of requested character.</param>
		/// <returns></returns>
		char this[int index] { get; }

		/// <summary>
		/// Returns true if this stringie is composed from one or more stringies.
		/// </summary>
		bool IsComposed { get; }

		/// <summary>
		/// Returns equivalent stringie which is not composed from another stringies.
		/// </summary>
		/// <returns></returns>
		IStringie Flatten();

		void ToString(StringBuilder builder);

		IStringie Clone();

		int AppendHashCode(int hashCode);
	}
}
