﻿using System;
using System.Collections.Generic;
using System.Text;
using Stringularity.Shared;
using Stringularity.Stringies;
using Stringularity.Stringies.Multis;

namespace Stringularity {
	public abstract class Stringie : IStringie {
		public const StringComparison DefaultStringComparision = StringComparison.InvariantCulture;

		private static Stringie emptyInstance;
		public static Stringie Empty => emptyInstance ?? (emptyInstance = new Empty());

		public static Stringie Create(string content) => content is null ? null : content.Length < 1 ? Empty : new OneString(content);
		public static Stringie Create(char content) => new OneChar(content);
		public static Stringie Create(char[] content) {
			if (content is null) { return null; }
			if (content.Length < 1) { return Empty; }
			if (content.Length == 1) { return new OneChar(content[0]); }

			return new CharArray(content);
		}
		public static Stringie Create(IReadOnlyList<char> content) {
			if (content is null) { return null; }

			var len = content.Count;
			if (len < 1) { return Empty; }
			if (len < 2) { return Create(content[0]); }

			return new CharArray(content);
		}
		public static Stringie Create(IStringie content) {
			if (content is Stringie stg) { return stg; }
			if (content is null) { return null; }

			return new OneStringie(content);
		}

		protected static string ToString(IReadOnlyList<char> chars) {
			var len = chars.Count;

			var arrChars = chars as char[];
			if (arrChars is null) {
				arrChars = new char[len];
				for (var i = 0; i < len; i++) {
					arrChars[i] = chars[i];
				}
			}

			return new string(arrChars);
		}

		#region Converts
		public static explicit operator string(Stringie obj) => obj?.ToString();

		public static implicit operator Stringie(string obj) => Create(obj);
		public static implicit operator Stringie(char obj) => Create(obj);
		public static implicit operator Stringie(char[] obj) => Create(obj);
		#endregion Converts

		#region Operator +
		public static Stringie operator +(Stringie a, Stringie b) => a?.Append(b) ?? b;

		public static Stringie operator +(Stringie a, IStringie b) => a?.Append(b) ?? Create(b);
		public static Stringie operator +(IStringie a, Stringie b) => Create(a?.Append(b)) ?? b;

		public static Stringie operator +(Stringie a, string b) => a?.Append(b) ?? Create(b);
		public static Stringie operator +(string a, Stringie b) => Create(a)?.Append(b) ?? b;

		public static Stringie operator +(Stringie a, char b) => a?.Append(b) ?? Create(b);
		public static Stringie operator +(char a, Stringie b) => Create(a)?.Append(b) ?? b;

		public static Stringie operator +(Stringie a, char[] b) => a?.Append(b) ?? Create(b);
		public static Stringie operator +(char[] a, Stringie b) => Create(a)?.Append(b) ?? b;

		public static Stringie operator +(Stringie a, IReadOnlyList<char> b) => a?.Append(b) ?? Create(b);
		public static Stringie operator +(IReadOnlyList<char> a, Stringie b) => Create(a)?.Append(b) ?? b;
		#endregion Operator +

		#region Append
		public virtual Stringie Append(char content) => this.Append(Create(content));
		IStringie IStringie.Append(char content) => this.Append(content);

		public virtual Stringie Append(char[] content) => this.Append(Create(content));
		IStringie IStringie.Append(char[] content) => this.Append(content);

		public virtual Stringie Append(IReadOnlyList<char> content) => this.Append(Create(content));
		IStringie IStringie.Append(IReadOnlyList<char> content) => this.Append(content);

		public virtual Stringie Append(string content) => this.Append(Create(content));
		IStringie IStringie.Append(string content) => this.Append(content);

		public virtual Stringie Append(IStringie content) {
			if (content is null || content.IsEmpty) { return this; }

			return new Stringies.Concats.ConcatedTwoStringies(this, content);
		}
		IStringie IStringie.Append(IStringie content) => this.Append(content);
		#endregion Append

		#region Prepend
		public virtual Stringie Prepend(char content) => this.Prepend(Create(content));
		IStringie IStringie.Prepend(char content) => this.Prepend(content);

		public virtual Stringie Prepend(char[] content) => this.Prepend(Create(content));
		IStringie IStringie.Prepend(char[] content) => this.Prepend(content);

		public virtual Stringie Prepend(IReadOnlyList<char> content) => this.Prepend(Create(content));
		IStringie IStringie.Prepend(IReadOnlyList<char> content) => this.Prepend(content);

		public virtual Stringie Prepend(string content) => this.Prepend(Create(content));
		IStringie IStringie.Prepend(string content) => this.Prepend(content);

		public virtual Stringie Prepend(IStringie content) {
			if (content is null || content.IsEmpty) { return this; }

			return new Stringies.Concats.ConcatedTwoStringies(content, this);
		}
		IStringie IStringie.Prepend(IStringie content) => this.Prepend(content);
		#endregion Prepend

		#region Operator -
		public static Stringie operator -(Stringie a, Stringie b) => throw new NotImplementedException();

		public static Stringie operator -(Stringie a, IStringie b) => throw new NotImplementedException();
		public static Stringie operator -(IStringie a, Stringie b) => throw new NotImplementedException();

		public static Stringie operator -(Stringie a, string b) => throw new NotImplementedException();
		public static Stringie operator -(string a, Stringie b) => throw new NotImplementedException();

		public static Stringie operator -(Stringie a, char b) => throw new NotImplementedException();
		public static Stringie operator -(char a, Stringie b) => throw new NotImplementedException();

		public static Stringie operator -(Stringie a, char[] b) => throw new NotImplementedException();
		public static Stringie operator -(char[] a, Stringie b) => throw new NotImplementedException();

		public static Stringie operator -(Stringie a, IReadOnlyList<char> b) => throw new NotImplementedException();
		public static Stringie operator -(IReadOnlyList<char> a, Stringie b) => throw new NotImplementedException();
		#endregion Operator -

		#region Operator *
		public static Stringie operator *(Stringie a, byte b) => throw new NotImplementedException();
		public static Stringie operator *(byte a, Stringie b) => throw new NotImplementedException();

		public static Stringie operator *(Stringie a, sbyte b) => throw new NotImplementedException();
		public static Stringie operator *(sbyte a, Stringie b) => throw new NotImplementedException();

		public static Stringie operator *(Stringie a, short b) => throw new NotImplementedException();
		public static Stringie operator *(short a, Stringie b) => throw new NotImplementedException();

		public static Stringie operator *(Stringie a, ushort b) => throw new NotImplementedException();
		public static Stringie operator *(ushort a, Stringie b) => throw new NotImplementedException();

		public static Stringie operator *(Stringie a, int b) => throw new NotImplementedException();
		public static Stringie operator *(int a, Stringie b) => throw new NotImplementedException();

		public static Stringie operator *(Stringie a, uint b) => throw new NotImplementedException();
		public static Stringie operator *(uint a, Stringie b) => throw new NotImplementedException();

		public static Stringie operator *(Stringie a, long b) => throw new NotImplementedException();
		public static Stringie operator *(long a, Stringie b) => throw new NotImplementedException();

		public static Stringie operator *(Stringie a, ulong b) => throw new NotImplementedException();
		public static Stringie operator *(ulong a, Stringie b) => throw new NotImplementedException();
		#endregion Operator *

		#region Operator /
		public static IEnumerable<Stringie> operator /(Stringie a, Stringie b) => throw new NotImplementedException();

		public static IEnumerable<Stringie> operator /(Stringie a, IStringie b) => throw new NotImplementedException();
		public static IEnumerable<Stringie> operator /(IStringie a, Stringie b) => throw new NotImplementedException();

		public static IEnumerable<Stringie> operator /(Stringie a, string b) => throw new NotImplementedException();
		public static IEnumerable<Stringie> operator /(string a, Stringie b) => throw new NotImplementedException();

		public static IEnumerable<Stringie> operator /(Stringie a, char b) => throw new NotImplementedException();
		public static IEnumerable<Stringie> operator /(char a, Stringie b) => throw new NotImplementedException();

		public static IEnumerable<Stringie> operator /(Stringie a, char[] b) => throw new NotImplementedException();
		public static IEnumerable<Stringie> operator /(char[] a, Stringie b) => throw new NotImplementedException();

		public static IEnumerable<Stringie> operator /(Stringie a, IReadOnlyList<char> b) => throw new NotImplementedException();
		public static IEnumerable<Stringie> operator /(IReadOnlyList<char> a, Stringie b) => throw new NotImplementedException();
		#endregion Operator /

		#region Basic members
		public abstract bool IsEmpty { get; }
		public abstract bool IsWhitespace { get; }
		public abstract int Length { get; }

		public abstract char this[int index] { get; }

		public abstract bool IsComposed { get; }

		public virtual Stringie Flatten() => this.IsComposed ? this.ToString() : this;
		IStringie IStringie.Flatten() => this.Flatten();

		public abstract void ToString(StringBuilder builder);

		public abstract Stringie Clone();
		IStringie IStringie.Clone() => this.Clone();

		protected abstract int AppendHashCode(int hashCode);
		int IStringie.AppendHashCode(int hashCode) => this.AppendHashCode(hashCode);
		#endregion Basic members

		#region Override object members
		public override string ToString() {
			var sb = new StringBuilder();
			this.ToString(sb);
			return sb.ToString();
		}
		public override int GetHashCode() => this.AppendHashCode(FnvHash.BaseCode);
		public override bool Equals(object obj) {
			if (obj is IStringie stg) { return Equals(stg); }
			if (obj is string str) { return Equals(str); }
			if (obj is char ch) { return Equals(ch); }
			if (obj is char[] arrCh) { return Equals(arrCh); }
			if (obj is IReadOnlyList<char> lstCh) { return Equals(lstCh); }
			if (obj is string[] arrStr) { return Equals(arrStr); }
			if (obj is IReadOnlyList<string> lstStr) { return Equals(lstStr); }

			return false;
		}
		#endregion Override object members

		#region IndexOf
		public virtual int IndexOf(char ch) => IndexOf(ch, DefaultStringComparision);
		public virtual int IndexOf(char[] chars) => IndexOf(chars, DefaultStringComparision);
		public virtual int IndexOf(IReadOnlyList<char> chars) => IndexOf(chars, DefaultStringComparision);
		public virtual int IndexOf(string str) => IndexOf(str, DefaultStringComparision);
		public virtual int IndexOf(IStringie str) => IndexOf(str, DefaultStringComparision);

		public abstract int IndexOf(char ch, StringComparison comparision);
		public abstract int IndexOf(char[] chars, StringComparison comparision);
		public abstract int IndexOf(IReadOnlyList<char> chars, StringComparison comparision);
		public abstract int IndexOf(string str, StringComparison comparision);
		public abstract int IndexOf(IStringie str, StringComparison comparision);
		#endregion IndexOf

		#region Contains
		public virtual bool Contains(char ch) => Contains(ch, DefaultStringComparision);
		public virtual bool Contains(char[] chars) => Contains(chars, DefaultStringComparision);
		public virtual bool Contains(IReadOnlyList<char> chars) => Contains(chars, DefaultStringComparision);
		public virtual bool Contains(string str) => Contains(str, DefaultStringComparision);
		public virtual bool Contains(IStringie str) => Contains(str, DefaultStringComparision);

		public virtual bool Contains(char ch, StringComparison comparision) => IndexOf(ch, comparision) >= 0;
		public virtual bool Contains(char[] chars, StringComparison comparision) => IndexOf(chars, comparision) >= 0;
		public virtual bool Contains(IReadOnlyList<char> chars, StringComparison comparision) => IndexOf(chars, comparision) >= 0;
		public virtual bool Contains(string str, StringComparison comparision) => IndexOf(str, comparision) >= 0;
		public virtual bool Contains(IStringie str, StringComparison comparision) => IndexOf(str, comparision) >= 0;
		#endregion Contains

		#region StartsWith
		public virtual bool StartsWith(char ch) => StartsWith(ch, DefaultStringComparision);
		public virtual bool StartsWith(char[] chars) => StartsWith(chars, DefaultStringComparision);
		public virtual bool StartsWith(IReadOnlyList<char> chars) => StartsWith(chars, DefaultStringComparision);
		public virtual bool StartsWith(string str) => StartsWith(str, DefaultStringComparision);
		public virtual bool StartsWith(IStringie str) => StartsWith(str, DefaultStringComparision);

		public abstract bool StartsWith(char ch, StringComparison comparision);
		public abstract bool StartsWith(char[] chars, StringComparison comparision);
		public abstract bool StartsWith(IReadOnlyList<char> chars, StringComparison comparision);
		public abstract bool StartsWith(string str, StringComparison comparision);
		public abstract bool StartsWith(IStringie str, StringComparison comparision);
		#endregion StartsWith

		#region EndsWith
		public virtual bool EndsWith(char ch) => EndsWith(ch, StringComparison.InvariantCulture);
		public virtual bool EndsWith(char[] chars) => EndsWith(chars, StringComparison.InvariantCulture);
		public virtual bool EndsWith(IReadOnlyList<char> chars) => EndsWith(chars, StringComparison.InvariantCulture);
		public virtual bool EndsWith(string str) => EndsWith(str, StringComparison.InvariantCulture);
		public virtual bool EndsWith(IStringie str) => EndsWith(str, StringComparison.InvariantCulture);

		public abstract bool EndsWith(char ch, StringComparison comparision);
		public abstract bool EndsWith(char[] chars, StringComparison comparision);
		public abstract bool EndsWith(IReadOnlyList<char> chars, StringComparison comparision);
		public abstract bool EndsWith(string str, StringComparison comparision);
		public abstract bool EndsWith(IStringie str, StringComparison comparision);
		#endregion EndsWith

		#region CompareTo
		public abstract int CompareTo(IStringie other);
		public abstract int CompareTo(string other);
		public abstract int CompareTo(char other);
		public abstract int CompareTo(char[] other);
		public abstract int CompareTo(IReadOnlyList<char> other);
		#endregion CompareTo

		#region Equals
		public virtual bool Equals(IStringie other) => CompareTo(other) == 0;
		public virtual bool Equals(string other) => CompareTo(other) == 0;
		public virtual bool Equals(char other) => CompareTo(other) == 0;
		public virtual bool Equals(char[] other) => CompareTo(other) == 0;
		public virtual bool Equals(IReadOnlyList<char> other) => CompareTo(other) == 0;
		#endregion Equals

		#region PadLeft
		public virtual Stringie PadLeft(char content, int width) {
			var diff = width - this.Length;
			if (diff < 1) { return this; }

			var padding = new MultipliedChar(content, diff);
			return this.Prepend(padding);
		}
		IStringie IStringie.PadLeft(char content, int width) => this.PadLeft(content, width);
		#endregion PadLeft

		#region PadRight
		public virtual Stringie PadRight(char content, int width) {
			var diff = width - this.Length;
			if (diff < 1) { return this; }

			var padding = new MultipliedChar(content, diff);
			return this.Append(padding);
		}
		IStringie IStringie.PadRight(char content, int width) => this.PadRight(content, width);
		#endregion PadRight
	}
}
