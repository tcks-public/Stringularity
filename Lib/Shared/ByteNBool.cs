﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity.Shared {
	public enum ByteNBool : byte {
		Null,
		False,
		True
	}
}
