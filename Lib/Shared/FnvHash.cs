﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity.Shared {
	public static class FnvHash {
		public const int BaseCode = unchecked((int)2166136261);
		public const int NextPrime = 16777619;

		public static int Append(int a, int b) {
			return (a ^ b) * NextPrime;
		}

		public static int Append(int hashCode, IStringie a, IStringie b) {
			var result = hashCode;
			result = a.AppendHashCode(result);
			result = b.AppendHashCode(result);
			return result;
		}
		public static int Append(int hashCode, string a, string b) {
			var result = hashCode;
			result = Append(result, a.GetHashCode());
			result = Append(result, b.GetHashCode());
			return result;
		}

		public static int Compute(char[] chars) => Append(BaseCode, chars);
		public static int Append(int hashCode, char[] chars) {
			var result = hashCode;
			for (var i = 0; i < chars.Length; i++) {
				var hash = chars[i].GetHashCode();
				result = (result ^ hash) * NextPrime;
			}

			return result;
		}

		public static int Compute(string chars) => Append(BaseCode, chars);
		public static int Append(int hashCode, string chars) {
			var result = hashCode;
			for (var i = 0; i < chars.Length; i++) {
				var hash = chars[i].GetHashCode();
				result = (result ^ hash) * NextPrime;
			}

			return result;
		}
	}
}
