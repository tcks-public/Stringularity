﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity {
	partial interface IStringie {
		bool Contains(char ch);
		bool Contains(char[] chars);
		bool Contains(IReadOnlyList<char> chars);
		bool Contains(string str);
		bool Contains(IStringie str);
		
		bool Contains(char ch, StringComparison comparision);
		bool Contains(char[] chars, StringComparison comparision);
		bool Contains(IReadOnlyList<char> chars, StringComparison comparision);
		bool Contains(string str, StringComparison comparision);
		bool Contains(IStringie str, StringComparison comparision);
	}
}
