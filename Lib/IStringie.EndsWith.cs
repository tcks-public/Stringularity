﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity {
	partial interface IStringie {
		bool EndsWith(char ch);
		bool EndsWith(char[] chars);
		bool EndsWith(IReadOnlyList<char> chars);
		bool EndsWith(string str);
		bool EndsWith(IStringie str);
		
		bool EndsWith(char ch, StringComparison comparision);
		bool EndsWith(char[] chars, StringComparison comparision);
		bool EndsWith(IReadOnlyList<char> chars, StringComparison comparision);
		bool EndsWith(string str, StringComparison comparision);
		bool EndsWith(IStringie str, StringComparison comparision);
	}
}
