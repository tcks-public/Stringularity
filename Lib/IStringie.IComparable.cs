﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity
{
	partial interface IStringie : IComparable<IStringie> { }
	partial interface IStringie : IComparable<string> { }
	partial interface IStringie : IComparable<char> { }
	partial interface IStringie : IComparable<char[]> { }
	partial interface IStringie : IComparable<IReadOnlyList<char>> { }
}
