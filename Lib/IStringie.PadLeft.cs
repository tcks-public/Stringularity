﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity {
	partial interface IStringie {
		IStringie PadLeft(char content, int width);
	}
}
