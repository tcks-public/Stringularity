﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity {
	partial interface IStringie {
		IStringie Prepend(char content);
		IStringie Prepend(char[] content);
		IStringie Prepend(IReadOnlyList<char> content);
		IStringie Prepend(string content);
		IStringie Prepend(IStringie content);
	}
}
