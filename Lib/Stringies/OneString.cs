﻿using System;
using System.Collections.Generic;
using System.Text;
using Stringularity.Shared;

namespace Stringularity.Stringies {
	public sealed class OneString : Basic {
		private readonly string content;

		public OneString(string content) {
			if (content is null) { throw new ArgumentNullException(nameof(content)); }
			if (content.Length < 1) { throw new ArgumentException("Non empty string is required.", nameof(content)); }

			this.content = content;
		}

		public override bool IsEmpty => false;

		public override bool IsWhitespace => string.IsNullOrWhiteSpace(content);

		public override int Length => content.Length;

		public override char this[int index] => content[index];

		public override bool IsComposed => false;

		public override Stringie Flatten() => this;

		public override Stringie Clone() => new OneString(content);

		protected override int AppendHashCode(int hashCode) => FnvHash.Append(hashCode, this.content);

		public override string ToString() => content;
		public override void ToString(StringBuilder builder) {
			builder.Append(this.content);
		}
	}
}
