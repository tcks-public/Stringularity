﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity.Stringies {
	public sealed class OneStringie : Stringie {
		private readonly IStringie content;

		public OneStringie(IStringie content) {
			this.content = content ?? throw new ArgumentNullException(nameof(content));
		}

		public override bool IsEmpty => this.content.IsEmpty;

		public override bool IsWhitespace => this.content.IsWhitespace;

		public override int Length => this.content.Length;

		public override char this[int index] => this.content[index];

		public override bool IsComposed => true;

		public override void ToString(StringBuilder builder) => this.content.ToString();
		public override Stringie Clone() => new OneStringie(this.content);
		protected override int AppendHashCode(int hashCode) => this.content.AppendHashCode(hashCode);

		public override int IndexOf(char ch, StringComparison comparision) => this.content.IndexOf(ch, comparision);
		public override int IndexOf(char[] chars, StringComparison comparision) => this.content.IndexOf(chars, comparision);
		public override int IndexOf(IReadOnlyList<char> chars, StringComparison comparision) => this.content.IndexOf(chars, comparision);
		public override int IndexOf(string str, StringComparison comparision) => this.content.IndexOf(str, comparision);
		public override int IndexOf(IStringie str, StringComparison comparision) => this.content.IndexOf(str, comparision);

		public override bool StartsWith(char ch, StringComparison comparision) => this.content.StartsWith(ch, comparision);
		public override bool StartsWith(char[] chars, StringComparison comparision) => this.content.StartsWith(chars, comparision);
		public override bool StartsWith(IReadOnlyList<char> chars, StringComparison comparision) => this.content.StartsWith(chars, comparision);
		public override bool StartsWith(string str, StringComparison comparision) => this.content.StartsWith(str, comparision);
		public override bool StartsWith(IStringie str, StringComparison comparision) => this.content.StartsWith(str, comparision);

		public override bool EndsWith(char ch, StringComparison comparision) => this.content.EndsWith(ch, comparision);
		public override bool EndsWith(char[] chars, StringComparison comparision) => this.content.EndsWith(chars, comparision);
		public override bool EndsWith(IReadOnlyList<char> chars, StringComparison comparision) => this.content.EndsWith(chars, comparision);
		public override bool EndsWith(string str, StringComparison comparision) => this.content.EndsWith(str, comparision);
		public override bool EndsWith(IStringie str, StringComparison comparision) => this.content.EndsWith(str, comparision);

		public override int CompareTo(IStringie other) => this.content.CompareTo(other);
		public override int CompareTo(string other) => this.content.CompareTo(other);
		public override int CompareTo(char other) => this.content.CompareTo(other);
		public override int CompareTo(char[] other) => this.content.CompareTo(other);
		public override int CompareTo(IReadOnlyList<char> other) => this.content.CompareTo(other);
	}
}
