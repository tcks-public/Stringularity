﻿using System;
using System.Collections.Generic;
using System.Text;
using Stringularity.Shared;

namespace Stringularity.Stringies.Multis {
	public sealed class MultipliedChar : Basic {
		private readonly char content;
		private readonly int multiplier;

		public MultipliedChar(char content, int multiplier) {
			if (multiplier < 2) { throw new ArgumentOutOfRangeException(nameof(multiplier)); }

			this.content = content;
			this.multiplier = multiplier;
		}

		public override bool IsEmpty => false;

		public override bool IsWhitespace => char.IsWhiteSpace(this.content);

		public override int Length => this.multiplier;

		public override char this[int index] {
			get {
				if (index < 0 || index >= this.multiplier) { throw new IndexOutOfRangeException(); }

				return content;
			}
		}

		public override bool IsComposed => false;

		public override void ToString(StringBuilder builder) {
			for (var i = 0; i < this.multiplier; i++) {
				builder.Append(this.content);
			}
		}
		public override Stringie Clone() => new MultipliedChar(this.content, this.multiplier);
		protected override int AppendHashCode(int hashCode) {
			var result = hashCode;
			for (var i = 0; i < this.multiplier; i++) {
				result = FnvHash.Append(result, this.content.GetHashCode());
			}
			return result;
		}
	}
}
