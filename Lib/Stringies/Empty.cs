﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stringularity.Stringies {
	public sealed class Empty : Stringie {
		public override bool IsEmpty => true;

		public override bool IsWhitespace => false;

		public override int Length => 0;

		public override char this[int index] => throw new IndexOutOfRangeException();

		public override bool IsComposed => false;

		public override Stringie Flatten() => this;

		public override Stringie Clone() => new Empty();

		public override int GetHashCode() => 0;
		protected override int AppendHashCode(int hashCode) => hashCode;
		public override void ToString(StringBuilder builder) {
			// do nothing
		}

		#region Append
		public override Stringie Append(char content) => Create(content) ?? this;
		public override Stringie Append(char[] content) => content is null || content.Length < 1 ? this : Create(content) ?? this;
		public override Stringie Append(IReadOnlyList<char> content) => content is null || content.Count < 1 ? this : Create(content) ?? this;
		public override Stringie Append(string content) => string.IsNullOrEmpty(content) ? this : Create(content) ?? this;
		public override Stringie Append(IStringie content) => content is null || content.IsEmpty ? this : Create(content) ?? this;
		#endregion Append

		public override int IndexOf(char ch, StringComparison comparision) => -1;
		public override int IndexOf(char[] chars, StringComparison comparision) => chars is object && chars.Length < 1 ? 0 : -1;
		public override int IndexOf(IReadOnlyList<char> chars, StringComparison comparision) => chars is object && chars.Count < 1 ? 0 : -1;
		public override int IndexOf(string str, StringComparison comparision) => str is object && str.Length < 1 ? 0 : -1;
		public override int IndexOf(IStringie str, StringComparison comparision) => str is object && str.Length < 1 ? 0 : -1;

		public override bool StartsWith(char ch, StringComparison comparision) => false;
		public override bool StartsWith(char[] chars, StringComparison comparision) => chars is object && chars.Length < 1;
		public override bool StartsWith(IReadOnlyList<char> chars, StringComparison comparision) => chars is object && chars.Count < 1;
		public override bool StartsWith(string str, StringComparison comparision) => str is object && str.Length < 1;
		public override bool StartsWith(IStringie str, StringComparison comparision) => str is object && str.IsEmpty;

		public override bool EndsWith(char ch, StringComparison comparision) => false;
		public override bool EndsWith(char[] chars, StringComparison comparision) => chars is object && chars.Length < 1;
		public override bool EndsWith(IReadOnlyList<char> chars, StringComparison comparision) => chars is object && chars.Count < 1;
		public override bool EndsWith(string str, StringComparison comparision) => str is object && str.Length < 1;
		public override bool EndsWith(IStringie str, StringComparison comparision) => str is object && str.IsEmpty;

		public override int CompareTo(IStringie other) => other is null ? 1 : other.IsEmpty ? 0 : -1;
		public override int CompareTo(string other) => other is null ? 1 : other.Length < 1 ? 0 : -1;
		public override int CompareTo(char other) => -1;
		public override int CompareTo(char[] other) => other is null ? 1 : other.Length < 1 ? 0 : -1;
		public override int CompareTo(IReadOnlyList<char> other) => other is null ? 1 : other.Count < 1 ? 0 : -1;

		public override bool Equals(IStringie other) => other is object && other.IsEmpty;
		public override bool Equals(string other) => other is object && other.Length < 1;
		public override bool Equals(char other) => false;
		public override bool Equals(char[] other) => other is object && other.Length < 1;
		public override bool Equals(IReadOnlyList<char> other) => other is object && other.Count < 1;
	}
}
