﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stringularity.Shared;

namespace Stringularity.Stringies {
	public sealed class CharArray : Basic {
		private readonly char[] content;

		public CharArray(char[] content) {
			if (content is null) { throw new ArgumentNullException(nameof(content)); }
			if (content.Length < 1) { throw new ArgumentException("Non empty content is required.", nameof(content)); }

			this.content = (char[])content.Clone();
		}

		public CharArray(IReadOnlyList<char> content) {
			if (content is null) { throw new ArgumentNullException(nameof(content)); }
			if (content.Count < 1) { throw new ArgumentException("Non empty content is required.", nameof(content)); }

			this.content = content.ToArray();
		}

		public override bool IsEmpty => false;

		private bool? isWhitespace;
		public override bool IsWhitespace {
			get {
				var result = this.isWhitespace;
				if (result.HasValue) { return result.Value; }

				for (var i = 0; i < this.content.Length; i++) {
					if (!char.IsWhiteSpace(this.content[i])) {
						this.isWhitespace = false;
						return false;
					}
				}

				this.isWhitespace = true;
				return true;
			}
		}

		public override int Length => this.content.Length;

		public override char this[int index] => this.content[index];

		public override bool IsComposed => false;

		public override Stringie Clone() => new CharArray(content);

		public override Stringie Flatten() => this;

		protected override int AppendHashCode(int hashCode) => FnvHash.Append(hashCode, this.content);

		public override void ToString(StringBuilder builder) {
			builder.Append(this.content);
		}
	}
}
