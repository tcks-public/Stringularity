﻿using System;
using System.Collections.Generic;
using System.Text;
using Stringularity.Shared;

namespace Stringularity.Stringies {
	public abstract class Basic : Stringie {
		#region GetHashCode
		private int hashCode;
		public override int GetHashCode() {
			var result = this.hashCode;
			if (result == 0) {
				this.hashCode = result = this.AppendHashCode(FnvHash.BaseCode);
			}

			return result;
		}
		#endregion GetHashCode

		#region IndexOf
		public override int IndexOf(char ch, StringComparison comparision) {
			// TODO: need optimalization with proper comparing, not creating new string
			return IndexOf(ch.ToString(), comparision);
		}
		public override int IndexOf(char[] chars, StringComparison comparision) {
			if (chars is null) { return -1; }
			if (chars.Length < 1) { return 0; }

			// TODO: need optimalization with proper comparing, not creating new string
			return IndexOf(new string(chars), comparision);
		}
		public override int IndexOf(IReadOnlyList<char> chars, StringComparison comparision) {
			if (chars is null) { return -1; }

			var len = chars.Count;
			if (len < 1) { return 0; }

			// TODO: need optimalization with proper comparing, not creating new string
			var str = ToString(chars);
			return IndexOf(str, comparision);
		}
		public override int IndexOf(IStringie str, StringComparison comparision) {
			if (str is null) { return -1; }
			if (str.IsEmpty) { return 0; }

			// TODO: need optimalization with proper comparing, not creating new string
			return IndexOf(str.ToString(), comparision);
		}
		public override int IndexOf(string str, StringComparison comparision) {
			if (str is null) { return -1; }
			if (str.Length < 1) { return 0; }

			// TODO: this is huge performance issue, but works
			var self = this.ToString();
			return self.IndexOf(str, comparision);
		}
		#endregion IndexOf

		#region StartsWith
		public override bool StartsWith(char ch, StringComparison comparision) {
			// TODO: need optimalization with proper comparing, not creating new string
			return StartsWith(ch.ToString(), comparision);
		}
		public override bool StartsWith(char[] chars, StringComparison comparision) {
			if (chars is null) { return false; }
			if (chars.Length < 1) { return true; }

			// TODO: need optimalization with proper comparing, not creating new string
			return StartsWith(new string(chars), comparision);
		}
		public override bool StartsWith(IReadOnlyList<char> chars, StringComparison comparision) {
			if (chars is null) { return false; }

			var len = chars.Count;
			if (len < 1) { return true; }

			// TODO: need optimalization with proper comparing, not creating new string
			var str = ToString(chars);
			return StartsWith(str, comparision);
		}
		public override bool StartsWith(IStringie str, StringComparison comparision) {
			if (str is null) { return false; }
			if (str.IsEmpty) { return true; }

			// TODO: need optimalization with proper comparing, not creating new string
			return this.StartsWith(str.ToString(), comparision);
		}
		public override bool StartsWith(string str, StringComparison comparision) {
			if (str is null) { return false; }
			if (str.Length < 1) { return true; }

			// TODO: this is huge performance issue, but works
			var self = this.ToString();
			return self.StartsWith(str);
		}
		#endregion StartsWith

		#region EndsWith
		public override bool EndsWith(char ch, StringComparison comparision) {
			// TODO: need optimalization with proper comparing, not creating new string
			return EndsWith(ch.ToString(), comparision);
		}
		public override bool EndsWith(char[] chars, StringComparison comparision) {
			if (chars is null) { return false; }
			if (chars.Length < 1) { return true; }

			// TODO: need optimalization with proper comparing, not creating new string
			return EndsWith(new string(chars), comparision);
		}
		public override bool EndsWith(IReadOnlyList<char> chars, StringComparison comparision) {
			if (chars is null) { return false; }

			var len = chars.Count;
			if (len < 1) { return true; }

			// TODO: need optimalization with proper comparing, not creating new string
			var str = ToString(chars);
			return EndsWith(str, comparision);
		}
		public override bool EndsWith(IStringie str, StringComparison comparision) {
			if (str is null) { return false; }
			if (str.IsEmpty) { return true; }

			// TODO: need optimalization with proper comparing, not creating new string
			return this.EndsWith(str.ToString(), comparision);
		}
		public override bool EndsWith(string str, StringComparison comparision) {
			if (str is null) { return false; }
			if (str.Length < 1) { return true; }

			// TODO: this is huge performance issue, but works
			var self = this.ToString();
			return self.EndsWith(str);
		}
		#endregion EndsWith

		#region CompareTo
		public override int CompareTo(IStringie other) {
			if (other is null) { return 1; }

			var lenA = this.Length;
			var lenB = other.Length;
			var len = lenA > lenB ? lenB : lenA;

			int result;
			// TODO: need optimization, the direct access of characters will not be always ideal
			char chA, chB;
			for (var i = 0; i < len; i++) {
				chA = this[i];
				chB = other[i];

				result = chA.CompareTo(chB);
				if (result != 0) { return result; }
			}

			if (lenA > lenB) {
				return 1;
			}
			else if (lenA < lenB) {
				return -1;
			}

			return 0;
		}
		public override int CompareTo(string other) {
			if (other is null) { return 1; }

			var lenA = this.Length;
			var lenB = other.Length;
			var len = lenA > lenB ? lenB : lenA;

			int result;
			// TODO: need optimization, the direct access of characters will not be always ideal
			char chA, chB;
			for (var i = 0; i < len; i++) {
				chA = this[i];
				chB = other[i];

				result = chA.CompareTo(chB);
				if (result != 0) { return result; }
			}

			if (lenA > lenB) {
				return 1;
			}
			else if (lenA < lenB) {
				return -1;
			}

			return 0;
		}
		public override int CompareTo(char other) {
			var len = this.Length;
			if (len < 1) { return -1; }

			var result = this[0].CompareTo(other);
			if (result == 0 && len > 1) {
				result = 1;
			}

			return result;
		}
		public override int CompareTo(char[] other) {
			if (other is null) { return 1; }

			var lenA = this.Length;
			var lenB = other.Length;
			var len = lenA > lenB ? lenB : lenA;

			int result;
			// TODO: need optimization, the direct access of characters will not be always ideal
			char chA, chB;
			for (var i = 0; i < len; i++) {
				chA = this[i];
				chB = other[i];

				result = chA.CompareTo(chB);
				if (result != 0) { return result; }
			}

			if (lenA > lenB) {
				return 1;
			}
			else if (lenA < lenB) {
				return -1;
			}

			return 0;
		}
		public override int CompareTo(IReadOnlyList<char> other) {
			if (other is null) { return 1; }

			var lenA = this.Length;
			var lenB = other.Count;
			var len = lenA > lenB ? lenB : lenA;

			int result;
			// TODO: need optimization, the direct access of characters will not be always ideal
			char chA, chB;
			for (var i = 0; i < len; i++) {
				chA = this[i];
				chB = other[i];

				result = chA.CompareTo(chB);
				if (result != 0) { return result; }
			}

			if (lenA > lenB) {
				return 1;
			}
			else if (lenA < lenB) {
				return -1;
			}

			return 0;
		}
		#endregion CompareTo
	}
}
