﻿using System;
using System.Collections.Generic;
using System.Text;
using Stringularity.Shared;

namespace Stringularity.Stringies.Concats {
	public sealed class ConcatedTwoStringies : Basic {
		private readonly IStringie first;
		private readonly IStringie second;

		public ConcatedTwoStringies(Stringie first, Stringie second) : this((IStringie)first, (IStringie)second) { }
		public ConcatedTwoStringies(IStringie first, IStringie second) {
			if (first is null) { throw new ArgumentNullException(nameof(first)); }
			if (second is null) { throw new ArgumentNullException(nameof(second)); }

			if (first.Length < 1) { throw new ArgumentException("Non empty content is required.", nameof(first)); }
			if (second.Length < 1) { throw new ArgumentException("Non empty content is required.", nameof(second)); }

			this.first = first;
			this.second = second;

			this.Length = first.Length + second.Length;
		}

		public override bool IsEmpty => false;

		public override bool IsWhitespace => this.first.IsWhitespace && this.second.IsWhitespace;

		public override int Length { get; }

		public override char this[int index] {
			get {
				var lenFirst = this.first.Length;
				if (index < lenFirst) {
					return this.first[index];
				}

				return this.second[index - lenFirst];
			}
		}

		public override bool IsComposed => true;

		protected override int AppendHashCode(int hashCode) => FnvHash.Append(hashCode, this.first, this.second);
		public override void ToString(StringBuilder builder) {
			this.first.ToString(builder);
			this.second.ToString(builder);
		}
		public override Stringie Clone() => new ConcatedTwoStringies(first, second);
	}
}
