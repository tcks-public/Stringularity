﻿using System;
using System.Collections.Generic;
using System.Text;
using Stringularity.Shared;

namespace Stringularity.Stringies.Concats {
	public sealed class ConcatedStringieArray : Basic {
		private readonly IStringie[] content;

		public ConcatedStringieArray(IStringie[] content) {
			if (content is null) { throw new ArgumentNullException(nameof(content)); }
			if (content.Length < 1) { throw new ArgumentException("Non-empty content is required.", nameof(content)); }

			bool allItemsAreEmpty = true;
			int length = 0;
			var arr = new IStringie[content.Length];
			for (var i = 0; i < arr.Length; i++) {
				var item = content[i] ?? throw new ArgumentNullException($"{nameof(content)}[{i}]");
				arr[i] = item;
				length += item.Length;
				if (allItemsAreEmpty && !item.IsEmpty) {
					allItemsAreEmpty = false;
				}
			}
			this.content = arr;
			this.IsEmpty = allItemsAreEmpty;
			this.Length = length;
		}

		public override bool IsEmpty { get; }

		private ByteNBool isWhitespace;
		public override bool IsWhitespace {
			get {
				var result = this.isWhitespace;
				if (result == ByteNBool.False) { return false; }
				if (result == ByteNBool.True) { return true; }
				if (this.IsEmpty) {
					this.isWhitespace = ByteNBool.False;
					return false;
				}

				for (var i = 0; i < content.Length; i++) {
					if (!content[i].IsWhitespace) {
						this.isWhitespace = ByteNBool.False;
						return true;
					}
				}

				this.isWhitespace = ByteNBool.True;
				return false;
			}
		}

		public override int Length { get; }

		public override char this[int index] {
			get {
				var offset = 0;
				foreach (var item in this.content) {
					var currIndex = index - offset;

					var currLength = item.Length;
					if (currIndex < currLength) {
						return item[currIndex];
					}

					offset += currLength;
				}

				throw new IndexOutOfRangeException();
			}
		}

		public override bool IsComposed => true;

		public override void ToString(StringBuilder builder) {
			foreach (var item in this.content) {
				item.ToString(builder);
			}
		}
		public override Stringie Clone() => new ConcatedStringieArray(this.content);
		protected override int AppendHashCode(int hashCode) {
			var result = hashCode;
			foreach (var item in this.content) {
				result = item.AppendHashCode(result);
			}

			return result;
		}
	}
}
