﻿using System;
using System.Collections.Generic;
using System.Text;
using Stringularity.Shared;

namespace Stringularity.Stringies.Concats {
	public sealed class ConcatedTwoStrings : Basic {
		private string first;
		private string second;

		public ConcatedTwoStrings(string first, string second) {
			if (first is null) { throw new ArgumentNullException(nameof(first)); }
			if (second is null) { throw new ArgumentNullException(nameof(second)); }

			this.first = first;
			this.second = second;

			this.Length = first.Length + second.Length;
		}

		public override bool IsEmpty => false;

		public override bool IsWhitespace => string.IsNullOrWhiteSpace(first) || string.IsNullOrWhiteSpace(second);

		public override int Length { get; }

		public override char this[int index] {
			get {
				var lenFirst = this.first.Length;
				if (index < lenFirst) {
					return this.first[index];
				}

				return this.second[index - lenFirst];
			}
		}

		public override bool IsComposed => true;

		public override void ToString(StringBuilder builder) => builder.Append(first).Append(second);
		public override string ToString() => string.Concat(first, second);
		public override Stringie Clone() => new ConcatedTwoStrings(first, second);
		protected override int AppendHashCode(int hashCode) => FnvHash.Append(hashCode, first, second);
	}
}
