﻿using System;
using System.Collections.Generic;
using System.Text;
using Stringularity.Shared;

namespace Stringularity.Stringies {
	public sealed class OneChar : Basic {
		private readonly char content;

		public OneChar(char content) {
			this.content = content;
		}

		public override bool IsEmpty => false;

		public override bool IsWhitespace => char.IsWhiteSpace(content);

		public override int Length => 1;

		public override char this[int index] => index != 0 ? throw new IndexOutOfRangeException() : content;

		public override bool IsComposed => false;

		public override Stringie Flatten() => this;

		public override Stringie Clone() => new OneChar(content);

		protected override int AppendHashCode(int hashCode) => FnvHash.Append(hashCode, this.content.GetHashCode());
		public override void ToString(StringBuilder builder) {
			builder.Append(this.content);
		}

		public override int IndexOf(string str, StringComparison comparision) {
			if (str is null) { return -1; }

			var len = str.Length;
			if (len < 1) { return 0; }
			if (len > 1) { return -1; }

			// TODO: need optimization for not creating string
			return content.ToString().IndexOf(str, comparision);
		}
		public override bool StartsWith(string str, StringComparison comparision) => IndexOf(str, comparision) == 0;
		public override bool EndsWith(string str, StringComparison comparision) => IndexOf(str, comparision) == 0;
	}
}
